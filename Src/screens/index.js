import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Splash from '../views/Splash';
import AuthView from '../views/AuthView';
import Login from '../views/Login';
import SignUP from '../views/SignUp'
import MyOrders from '../views/MyOrders';
import OrderPlaced from '../views/orderPlaced';
import EditProfile from '../views/EditProfile';
import ShareCredit from '../views/ShareCredit';
import MyProfile from '../views/MyProfile';
import CreditView from '../views/CreditView';
import BuyCredit from '../views/BuyCredit';
import ContactSupport from '../views/ContactSupport';
import GiftReceived from '../views/GiftReceived';
import ForwardGift from '../views/ForwardGift';
import YourCards from '../views/YourCards';
import MyCart from '../views/MyCart';
import CountryLis from '../views/CountryList';
import Home from '../views/Home'
import SearchFlower from '../views/SearchFlowers';
import Categories from '../views/Categories';
import Detail from '../views/Detail';

import { Icon } from 'react-native-elements/dist/icons/Icon';
import { darkGrey, grey } from '../constants/colors';
import { Image } from 'react-native';
import {profile_sel , profile_unsel , cart_sel, cart_unsel , home_sel, home_unsel, cate_sel, cate_unsel} from '../constants/images'



const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function SplashStack(){
  return (
      <Stack.Navigator initialRouteName={"Splash"} screenOptions={{ headerShown: false }}>
          <Stack.Screen name={'Splash'} component={Splash} />
      </Stack.Navigator>
  )
}

function LandingStack(){
    return (
        <Stack.Navigator initialRouteName={"AuthView"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'AuthView'} component={AuthView} />
        </Stack.Navigator>
    )
}

function AuthStack(){
  return (
      <Stack.Navigator initialRouteName={"Login"} screenOptions={{ headerShown: false }}>
          <Stack.Screen name={'SignUP'} component={SignUP} />
          <Stack.Screen name={'Login'} component={Login} />
          <Stack.Screen name={'ContactSupport'} component={ContactSupport} />
      </Stack.Navigator>
  )
}

function AppStack(){
  return(
    <Stack.Navigator initialRouteName={"Detail"} screenOptions={{ headerShown: false }}>
          <Stack.Screen name={'Home'} component={Home} />
          <Stack.Screen name={'Detail'} component={Detail} />
          <Stack.Screen name={'SearchFlower'} component={SearchFlower} />
          <Stack.Screen name={'CreditView'} component={CreditView} />
          <Stack.Screen name={'BuyCredit'} component={BuyCredit} />
          <Stack.Screen name={'CountryLis'} component={CountryLis} />
          <Stack.Screen name={'OrderPlaced'} component={OrderPlaced} />
          <Stack.Screen name={'YourCards'} component={YourCards} />
          <Stack.Screen name={'ForwardGift'} component={ForwardGift} />
          <Stack.Screen name={'GiftReceived'} component={GiftReceived} />
          <Stack.Screen name={'ShareCredit'} component={ShareCredit} />
          <Stack.Screen name={'MyOrders'} component={MyOrders} />
          <Stack.Screen name={'EditProfile'} component={EditProfile} />

    </Stack.Navigator>

  )
}

function BottomTabs() {
  return (
    <Tab.Navigator
      initialRouteName={'Home'} screenOptions={{ headerShown: false }}>
      <Tab.Screen name="Home" component={Home}
        options={{tabBarShowLabel:false, tabBarLabel: 'Home',
          tabBarIcon: ({ focused, color, size }) => (
            <Image
            source={focused? home_sel:home_unsel}
            style={{width:20,height:20,resizeMode:'contain'}}
            />
            )}}
       />
      <Tab.Screen name="Categories" component={Categories}
        options={{tabBarShowLabel:false, tabBarLabel:'Chat',
          tabBarIcon: ({ focused, color, size }) => (
            <Image
            source={focused? cate_sel:cate_unsel}
            style={{width:20,height:20,resizeMode:'contain'}}
            />
            )}}
       />
      <Tab.Screen name="MyCart" component={MyCart}
        options={{tabBarShowLabel:false, tabBarLabel:'Cart',
          tabBarIcon: ({ focused, color, size }) => (
            <Image
            source={focused? cart_sel:cart_unsel}
            style={{width:20,height:20,resizeMode:'contain'}}
            />
            )}}
       />
      <Tab.Screen name="MyProfile" component={MyProfile}
        options={{ tabBarShowLabel:false, tabBarLabel:'Profile',
          tabBarIcon: ({ focused, color, size }) => (
            <Image
            source={focused? profile_sel:profile_unsel}
            style={{width:20,height:20,resizeMode:'contain'}}
            />
            )}}
       /> 
    </Tab.Navigator>
  );
}


function AppContainer() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashSreen" screenOptions={{ headerShown: false }}>
        {/* <Stack.Screen name={'GiftReceived'} component={GiftReceived} /> */}
          <Stack.Screen name={'SplashSreen'} component={SplashStack} />
          <Stack.Screen name={'LandingScreen'} component={LandingStack} />
          <Stack.Screen name={'AuthStack'} component={AuthStack} />
          <Stack.Screen name={'BottomTabs'} component={BottomTabs} />
         <Stack.Screen name={'AppStack'} component={AppStack} />


        </Stack.Navigator>
      </NavigationContainer>
    )
  }
  
  export default AppContainer
