import React from 'react'
import { Platform , StyleSheet , Text} from 'react-native';
import { View } from 'react-native-animatable';
import { Overlay } from 'react-native-elements'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { white } from '../constants/colors';
import SignUp from '../views/SignUp';

const SignUpOverLay =(props)=>{
    const {isVisible} = props
    return(
        <Overlay
        isVisible={isVisible ? isVisible : false}
        useNativeDriver={true}
        onBackdropPress={props.onBackdropPress}
        animationIn={props.animationIn}
        animationInTiming={props.animationInTiming}
        animationOut={props.animationOut}
        overlayStyle={styles.overlayStyle}
        fullScreen={true}
    >
        <View style={styles.modalView}>
         <SignUp
          openLogin={props.openLogin}
          openSignUp={props.openSignUp}
         />
        </View>
    </Overlay>
    )
}
export default SignUpOverLay

const styles = StyleSheet.create({
    overlayStyle: {
        backgroundColor: 'transparent',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 22,
        borderBottomLeftRadius: 22,
        // width:wp(100),
        alignSelf: 'center',
        paddingLeft: 0,
        paddingTop: 0,
        paddingRight: 0,
        marginBottom: Platform.OS == 'android' ? 50 : 0,
    },
    modalView: {
        backgroundColor:white,
        width: wp(100),
        height: hp(100),
        // paddingTop: Platform.OS == 'android' ? 20 : isIphoneX() ? 60 : 35,

    },
})