import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Icon } from 'react-native-elements';
import { TouchableOpacity } from 'react-native';

import { regular } from '../constants/fontsFamily';
import { black } from '../constants/colors';
import { large, tiny } from '../constants/appFontSize';



const BackButton = (props) => {
    const { title, onPress, text } = props
    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.7}
            style={{ width: wp(20) }}
        >
            <View style={styles.wraper}>
                <Icon
                    name={'left'}
                    type={'antdesign'}
                    iconStyle={styles.iconStyle}
                    tvParallaxProperties={undefined}
                />
                <Text style={styles.textStyle}>{text}</Text>
            </View>
        </TouchableOpacity>
    )
}
export default BackButton

const styles = StyleSheet.create({
    wraper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconStyle: {
        color: black,
        fontSize: large,
    },
    textStyle: {
        color: black,
        fontFamily: regular,
        fontSize: tiny,
        marginLeft: 5,
    }
})