import React from 'react'
import {
    View,
    TextInput,
    StyleSheet,
    Platform,
    Image
} from 'react-native'
import { Icon } from 'react-native-elements'
import {  darkGrey, lightGrey, red } from '../constants/colors'



const InputField = React.forwardRef((props, ref) => {
    const { iconName, iconType, passIconName, passIconType, hideShowPassword, image , error } = props

    return (
        <View style={
           error? [
         styles.wraper,{borderColor:red}, 
         props.inputContainerStyle
        ]:
            [styles.wraper, 
            props.inputContainerStyle]
            }>
            {iconName ?
                <Icon
                    tvParallaxProperties={undefined}
                    name={iconName}
                    type={iconType}
                    iconStyle={styles.passIcon}
                />
                : image ?
                    <Image
                        source={image}
                        style={styles.imageStyle}
                    /> :
                    null}
            <TextInput
                ref={ref}
                underlineColorAndroid="transparent"
                secureTextEntry={props.secureTextEntry}
                editable={props.editable}
                keyboardType={props.keyboardType}
                placeholder={props.placeholder}
                placeholderTextColor={props.placeholderTextColor || error ?red:'grey'}
                onChangeText={props.onChangeText}
                onSubmitEditing={props.onSubmitEditing}
                value={props.value}
                returnKeyType={props.returnKeyType}
                multiline={props.multiline}
                numberOfLines={props.numberOfLines}
                textAlignVertical={props.textAlignVertical}
                style={[styles.Input, props.InputStyle]}
            />
            <Icon
                name={passIconName}
                tvParallaxProperties={undefined}
                type={passIconType}
                iconStyle = {passIconName=='eye'? {...styles.passIcon,color:darkGrey} :styles.passIcon}
                onPress={hideShowPassword}
            />
        </View>
    )

}
)

export default InputField

const styles = StyleSheet.create({
    wraper: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor:lightGrey,
        borderBottomWidth: 1.5,
    },

    icon: {
        fontSize: 20,
        color: 'red',
    },
    passIcon: {
        fontSize: 20,
        color:darkGrey
    },
    Input: {
        flex: 1,
        fontSize: 14,
        paddingHorizontal:0,
        paddingVertical: Platform.OS == 'android' ? 8 : 12,
    },
    imageStyle: {
        width: 18,
        height: 18
    }
})

