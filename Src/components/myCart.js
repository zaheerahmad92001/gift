import React from 'react'
import { View,Image, StyleSheet, Pressable, TouchableOpacity } from 'react-native'

import { maskgroup,calendar, van } from '../constants/images'
import { black, darkGrey, grey, lightBlue, red, white } from '../constants/colors'
import { Text } from 'react-native-animatable'
import { medium, regular, semi_bold } from '../constants/fontsFamily'
import { big_tiny, mediumSize, small, tiny } from '../constants/appFontSize'
import HeaderText from './headerText'
import { Icon } from 'react-native-elements/dist/icons/Icon'
import { heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { domain } from '../Utility'

const Cart = (props) => {
    const {
         price ,date, 
         name ,
          cartData,
          index,
          status,
          onPress ,
         handleDelete,
         handleDecrement,
         handleIncrement,
         defaultLocal
        } = props
    return (
        <>
        <Pressable 
        onPress={onPress}
        style={styles.wraper}>
          <View style={styles.contentView}>
              <View style={styles.imgStyle}>
              <Image
                source={{uri:`${domain}${cartData?.product?.attachments[0]?.url}`}}
                style={{ width: undefined, height: undefined, flex: 1, }}
              />
              </View>
              <View style={styles.leftView}>
                  <Text style={styles.nameText}>{cartData?.product?.title}</Text>
                  <Text style={styles.priceText}>{`$ ${cartData?.product?.price}`}</Text>
                   <Text style={styles.nameText}>{`${defaultLocal.quantity}`}</Text>
                   <View style={styles.QtyView}>
                       <View style={styles.qtyInner}>
                          <Pressable 
                           onPress={handleDecrement}
                           style={styles.circle}>
                              <Icon name='minus' type='antdesign'
                               iconStyle={styles.iconStyle}
                              />
                          </Pressable>
                          <Text style={{...styles.nameText,marginHorizontal:10,}}>{cartData?.quantity}</Text>
                          <Pressable 
                            onPress={handleIncrement}
                            style={{...styles.circle,backgroundColor:red}}>
                              <Icon name='plus'type='antdesign'
                              iconStyle={styles.iconStyle}
                              />
                          </Pressable>
                       </View>

                       <Icon
                         onPress={()=>handleDelete(cartData , index)}
                         name='delete'
                         type='material-community'
                         iconStyle={styles.deleteIcon}
                       />
                   </View>
              </View>
          </View>
        </Pressable>
        </>
    )
}
export default Cart

const styles = StyleSheet.create({
    wraper: {
        shadowColor:red,
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        paddingVertical:5,  
        marginBottom:hp(2),
        // marginHorizontal:1
    },
    contentView:{
     flexDirection:'row',
     alignItems:'center',
    },
        imgStyle: {
            width: 90,
            height:90,
            overflow: 'hidden',
            borderRadius:10,
            alignSelf:'flex-start',
        },
   leftView:{
       alignSelf:'flex-start',
       flex:1,
       marginTop:3,
       marginLeft:7,
    },
    nameText:{
        color:black,
        fontFamily:medium,
        fontSize:big_tiny,
    },
    priceText:{
        color:red,
        fontFamily:regular,
        fontSize:small,
    },
    smallImg:{
        width:20,
        height:20,
        resizeMode:'contain'
    },
    tinyHeading:{
    color:darkGrey,
    fontFamily:semi_bold,
    fontSize:tiny,
    marginLeft:5,
    },
    detailView:{
        flexDirection:'row',
        alignItems:'center',

    },
    dateText:{
        color:black,
        fontFamily:regular,
        fontSize:tiny, 
        left:3,
    },
    QtyView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    qtyInner:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:hp(1),
        marginBottom:10,
    },
    circle:{
        width:20,
        height:20,
        borderRadius:20/2,
        backgroundColor:grey,
        alignItems:'center',
        justifyContent:'center',
    },
    iconStyle:{
        color:white,
        fontSize:15,
    },
    deleteIcon:{
      fontSize:25,
      color:red
    },
})