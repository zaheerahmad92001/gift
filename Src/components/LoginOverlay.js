import React from 'react'
import { Platform , StyleSheet , Text} from 'react-native';
import { View } from 'react-native-animatable';
import { Overlay } from 'react-native-elements'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { white } from '../constants/colors';
import Login from '../views/Login';
import SignUp from '../views/SignUp';

const LoginOverLay =(props)=>{
    const {isVisible , showLogin , showSignUp} = props
    return(
        <Overlay
        isVisible={isVisible ? isVisible : false}
        useNativeDriver={true}
        onBackdropPress={props.onBackdropPress}
        animationIn={props.animationIn}
        animationInTiming={props.animationInTiming}
        animationOut={props.animationOut}
        overlayStyle={styles.overlayStyle}
        fullScreen={true}
    >
        <View style={styles.modalView}>
        {showLogin &&
         <Login
          openLogin={props.openLogin}
          openSignUp={props.openSignUp}
         />
         }
         {showSignUp &&
         <SignUp 
         />
        }
        </View>
    </Overlay>
    )
}
export default LoginOverLay

const styles = StyleSheet.create({
    overlayStyle: {
        backgroundColor: 'transparent',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 22,
        borderBottomLeftRadius: 22,
        // width:wp(100),
        alignSelf: 'center',
        paddingLeft: 0,
        paddingTop: 0,
        paddingRight: 0,
        marginBottom: Platform.OS == 'android' ? 50 : 0,
    },
    modalView: {
        backgroundColor:white,
        width: wp(100),
        height: hp(100),
        // paddingTop: Platform.OS == 'android' ? 20 : isIphoneX() ? 60 : 35,

    },
})