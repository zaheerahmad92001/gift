import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { View } from 'react-native-animatable'
import { Dropdown } from 'react-native-material-dropdown'
import { RFValue } from 'react-native-responsive-fontsize'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { small } from '../constants/appFontSize'
import { black, darkGrey, grey, red, white } from '../constants/colors'



const _DropDown = (props) => {
    const{placeholderColor} = props
    return (
        <Dropdown
            useNativeDriver={true}
            dropdownOffset={{ top: 0 }}
            // labelFontSize={12}
            // fontSize={50}
            pickerStyle={props.pickerStyle}
            baseColor={darkGrey}
            textColor={props.placeholder? placeholderColor : black}
            renderAccessory={props.renderAccessory}
            containerStyle={styles.card}
            // containerStyle={
            //     [styles.d_containerStyle,props.style]}
            inputContainerStyle={{ borderBottomWidth:0,marginBottom:-4 }}
            itemTextStyle={props.itemTextStyle}
            label=''
            style={StyleSheet.flatten([props.style,{fontSize:small,marginLeft:3, }]) }
            data={props.data}
            value={props.selectedValue ? props.selectedValue : props.staticValue}
            onChangeText={props.onChangeText}
            dropdownPosition={props.dropdownPosition}
            dropdownWidth={props.dropdownWidth}
            dropdownMargins={{min:props?.margin??5}}
            // pickerStyle={{borderBottomWidth:0}}
           
          />
    )

}
export default _DropDown;

const styles = StyleSheet.create({
    d_containerStyle: {
        paddingVertical:0,
        marginTop:0,
        paddingLeft: 0,
        borderRadius: 5,
        borderBottomWidth:0,
        justifyContent:'center',
        
    },

    d_containerErrorStyle: {
        backgroundColor:'transparent',
        paddingVertical:0,
        marginTop: 0,
        paddingLeft: 0,
        borderRadius: 5,
        borderBottomWidth:0
    },
    card:{
        backgroundColor:white,
        shadowColor:red,
        shadowOffset:{height:2,width:2},
        shadowOpacity:0.3,
        elevation:2,
        width:wp(24),
        marginLeft:10,
    }
})