import React from 'react'
import {Text , StyleSheet} from 'react-native'

import { lightBlack } from '../constants/colors'
import { regular } from '../constants/fontsFamily'
import { small } from '../constants/appFontSize'


const HeaderText =(props)=>{
    return(
     <Text style={[styles.text, props.textStyle]}>{props.text}</Text>
        
    )
}
export default HeaderText

const styles = StyleSheet.create({
    text:{
        textAlign:'center',
        fontFamily:regular,
        fontSize:small,
        color:lightBlack,
    }
})
