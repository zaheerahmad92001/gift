import { Icon } from 'react-native-elements';
import React from 'react'
import { View, Text, StyleSheet, Pressable } from 'react-native'
import DatePicker from 'react-native-date-picker';
import Modal from 'react-native-modal';
import { RFValue } from 'react-native-responsive-fontsize';
import { black, ferozi, white } from '../constants/colors';
import { regular } from '../constants/fontsFamily';
import { mediumSize } from '../constants/appFontSize';

export const _DatePicker = (props) => {
    const {mode , minimumDate ,} = props
    return (
        <Modal
            isVisible={props.isVisible}
            useNativeDriver={true}
        >
            <View style={styles.wraper}>
                <View style={styles.header}>
                    <Icon
                        onPress={props.onClose}
                        name={props.iconName ? props.iconName : 'close'}
                        type={props.iconType ? props.iconType : 'AntDesign'}
                        style={styles.iconStyle}
                    />
                </View>

                <DatePicker
                    date={props.date}
                    onDateChange={props.onDateChange}
                    // mode='time'
                    mode={mode? mode : "date"}
                    minimumDate={minimumDate}
                    minuteInterval={30}
                    style={{ position: 'relative', marginVertical: 20 }}
                />
                <Pressable 
                    onPress={props.onSelect}
                    style={styles.buttonStyle}>
                    <Text style={styles.btnText}>Ok</Text>
                </Pressable>
            </View>

        </Modal>
    )
}
export default _DatePicker;
const styles = StyleSheet.create({
    wraper: {
        backgroundColor: white,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        // paddingBottom: 10,
    },
    header: {
        backgroundColor:ferozi,
        height: RFValue(40),
        width: 'auto',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        justifyContent: 'center'
    },
    titleStyle: {
        fontSize: 15,
        color:white
    },
    iconStyle: {
        color: white,
        fontSize: 25,
        alignSelf: 'flex-end',
        marginRight: 10,

    },
    buttonStyle: {
        // marginHorizontal:20,
        backgroundColor:ferozi,
        paddingVertical:20,
        justifyContent:'center',
        alignItems:'center',
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
    },
    btnText:{
        color:black,
        fontFamily:regular,
        fontSize:mediumSize
    }
})