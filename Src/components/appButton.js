import React,{FC} from 'react'
import {Text, StyleSheet, Pressable } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


import { regular } from '../constants/fontsFamily';
import { mediumSize, small } from '../constants/appFontSize';
import { linDarkRed, linLighRed,white } from '../constants/colors';

const AppButton = (props) => {
    const { title, onPress ,btnColor1,btnColor2 ,disabled } = props
    return (
        <Pressable 
        style={[styles.btnStyle]}
        disabled={disabled}
        onPress={onPress}>
            <LinearGradient
                start={{ x:0.1, y: 0.1 }} end={{ x:1, y:1 }}
                colors={[btnColor1 || linLighRed ,btnColor2 || linDarkRed]}
                style={[styles.linearGradient, props.gradientBtnStyle]}>
                <Text style={[styles.buttonText, props.btnText]}>{title}</Text>
            </LinearGradient>
        </Pressable>
    )
}
export default AppButton

const styles = StyleSheet.create({
    linearGradient: {
        alignSelf: 'center',
        width: wp(60),
        borderRadius: 25,

    },
    buttonText: {
        fontSize:mediumSize,
        fontFamily:regular,
        textAlign: 'center',
        margin: 10,
        color:white,
        backgroundColor: 'transparent',
    },
    btnStyle:{
        width: wp(40),
        alignSelf: 'center',
    }
})