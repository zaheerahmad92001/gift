import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { regular, semi_bold } from '../constants/fontsFamily';
import { black, darkBlue, ferozi, red, white } from '../constants/colors';
import { headerHeigh, mediumSize, small } from '../constants/appFontSize';
import { Flag } from 'react-native-svg-flagkit'
import _DropDown from './dropDown';

const AppHeader = (props) => {
    const { 
        title,
        iconName, 
        iconType, 
        leftPress,
        rightIconName,
        rightIconType,
        rightPress,
        dropdown,
        placeholder
     } = props
    return (
        <View style={styles.wraper}>
      
      <View style={styles.left}>
      {iconName &&
         <Icon
          onPress={leftPress}
          name={iconName}
          type={iconType}
          tvParallaxProperties={undefined}
          iconStyle={styles.iconStyle}
         />
         }
         {dropdown ?
         <View style={styles.row}>
          <Flag id={dropdown} size={0.1}/>
         <Text style={styles.cname}>{dropdown}</Text>
         </View>
         : null
         }

         {/* {dropdown &&

        <_DropDown
            data={props.data}
            selectedValue={props.selectedValue}
            staticValue={'Country'}
            error={props.error}
            placeholder={placeholder}
            dropdownPosition={-4.5}
            style={{ backgroundColor: 'transparent', width:wp(30) }}
            pickerStyle={{marginLeft:wp(2), width:wp(30) }}
                //   onChangeText={(value, index, data) => this.petAge_month(value, index, data)}
            />
            } */}

      </View>
         
         <View style={styles.body}>
         <Text style={styles.centerText}>{title}</Text>
         </View>
         <View style={styles.right}>
         {rightIconName &&
         <View style={styles.rightView}>
         <Icon
          onPress={rightPress}
          name={rightIconName}
          type={rightIconType}
          tvParallaxProperties={undefined}
          iconStyle={[styles.iconStyle,{left:10,color:white}]}
         />
         </View>
         }
         </View>
         
        </View>
    )
}
export default AppHeader

const styles = StyleSheet.create({
    wraper:{
      marginTop:headerHeigh,
      marginHorizontal:10,
      flexDirection:'row',
      alignItems:'center',
    },
    left:{
       width:wp(15),
    },

    iconStyle:{
      fontSize:20,
      color:black,
      width:wp(10),
    },
    body:{
        width:wp(65)
    },
    centerText:{
        textAlign:'center',
        color:darkBlue,
        fontFamily:semi_bold,
        fontSize:mediumSize,
    },
    right:{
        width:wp(15),
    },
    rightView:{
        backgroundColor:ferozi,
        width:30,
        height:30,
        borderRadius:30/2,
        justifyContent:'center',
        // alignItems:'center',
        alignSelf:'center',
    },
    row:{
        flexDirection:"row",
         alignItems:"center",
        //  justifyContent:'center',
         backgroundColor:white,
            shadowColor:red,
            shadowOffset:{height:2,width:2},
            shadowOpacity:0.3,
            elevation:2,
            width:wp(24),
            marginLeft:10,

        },
        cname:{
            marginLeft:5,
            fontFamily:regular,
            fontSize:small,
            color:black
            
        },
        // card:{
        //     backgroundColor:white,
        //     shadowColor:red,
        //     shadowOffset:{height:2,width:2},
        //     shadowOpacity:0.3,
        //     elevation:2,
        //     width:wp(24),
        //     marginLeft:10,
        // }
})