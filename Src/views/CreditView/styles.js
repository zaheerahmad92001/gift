import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { grey, pink, white } from '../../constants/colors'
import { big_tiny, mediumSize, semi_bold, tiny } from '../../constants/appFontSize'
import { appMarginHorizontal } from '../../constants/appFontSize'
import { regular } from '../../constants/fontsFamily';



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1
    },
   
    profileView: {
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileOuterView: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor:pink,
        justifyContent: 'center',
        overflow: 'hidden',
        alignItems: 'center'
    },
    imgStyle: {
        width: 100,
        height: 100,
    },

    iconStyle: {
        color: white,
        fontSize: 20,
    },
    gradientBtnStyle: {
        width: wp(40),
        marginTop: hp(7),
    },
    textStyle:{
        marginTop:hp(3)
    },
    suggested:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:hp(3),
        alignSelf:'center',
    },
    imgView:{
        width:50,
        height:50,
        overflow:'hidden',
        alignSelf:'flex-start',
    },
    suggestedUserImg:{
        flex:1,
        height:null,
        width:undefined
    },
    userName:{
        color:grey,
        fontFamily:regular,
        fontSize:big_tiny
    },
    nameView:{
     marginLeft:15,
    },
    emailStyle:{
        color:grey,
        fontFamily:regular,
        fontSize:tiny
    },
    enterAmount:{
        alignSelf:'flex-start',
        fontSize:semi_bold,
        fontSize:mediumSize,
        marginTop:hp(2),
    },
    textView: {
        shadowColor:'#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        marginTop:hp(3),
        width:wp(50),
        alignSelf:'center',
    },
    textInput:{
        paddingLeft:10,
        flex:1,
        paddingVertical:12,
    }


})
export default styles