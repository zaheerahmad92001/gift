import React, { useReducer, useRef } from 'react'
import { View, Pressable, Image,Text , TextInput } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles'
import { Icon } from 'react-native-elements'
import { linDarkFerozi, linLightFerozi, red,grey } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import HeaderText from '../../components/headerText'
import {ladyPic} from '../../constants/images'
import AppButton from '../../components/appButton'


function CreditView({ navigation }) {

    const emailRef = useRef(null)
    const addressRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            filePath: '',
            name: undefined,
            email: undefined,
            contact: undefined,
            address: undefined,
        }
    )
    const { filePath, email } = state


    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.profileOuterView}>
                    {!filePath ?
                        <Icon
                            name="sharealt"
                            type={'antdesign'}
                            iconStyle={{ fontSize: 40, color: red }} />
                        :
                        <View style={styles.imgStyle}>
                            <Image
                                source={{ uri: filePath.uri }}
                                style={{ width: undefined, height: undefined, flex: 1 }} />
                        </View>
                    }
                </View>
            </View>
        )
    }
function suggestedUser(){
    return(
        <Pressable>
        <View style={styles.suggested}>
       <View style={styles.imgView}>
          <Image source={ladyPic} style={styles.suggestedUserImg}/>
       </View>
       <View style={styles.nameView}>
           <Text style={styles.userName}>Anna Williams</Text>
           <HeaderText text={'anna@email.com'} textStyle={styles.emailStyle}/>
       </View>
        </View>
       </Pressable>
    )
}

    function InFoUI() {
        return (
            <View style={styles.inputView}>
                <HeaderText
                text={'Enter Amount to Share'}
                textStyle={styles.enterAmount}
                />
                 {suggestedUser()}  
                 <View style={styles.textView}>
                  <TextInput
                  placeholder='Amount here'
                  placeholderTextColor={grey}
                  style={styles.textInput}
                  />
                 </View>
                <AppButton
                    title='SHARE'
                    onPress={() => { }}
                    btnColor1={linLightFerozi}
                    btnColor2={linDarkFerozi}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
            </View>
        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title='Share Credit'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {InFoUI()}
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default CreditView
