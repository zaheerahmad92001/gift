import React, { useReducer, useRef , useEffect } from 'react'
import { View, Image, Text, FlatList , TextInput, Pressable, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios'

import {authKeys, defaultLocal, local} from '../../constants/constantsValues'
import AppHeader from '../../components/appHeader'
import styles from './styles'
import {filter , search , flowers} from '../../constants/images'
import { lightGrey,red } from '../../constants/colors';
import { baseURL , domain } from '../../Utility';

const imgHeight = 180

function SearchFlower({ navigation,route}) {

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            productList:undefined,
            productListCopy:undefined,
            searchValue:undefined,
            accessKeys:undefined,
            loading:false,
            _local: '',
            _defaultLocal:'',

        }
    )
    const { 
        searchValue,productList ,
        accessKeys,
        productListCopy,
         loading ,
         _local,
        _defaultLocal,

        } = state

    useEffect(()=>{

        getKeys(getProducts)
        getDefaultLoca()

      },[])

    async function getKeys(callback){
        await AsyncStorage.getItem(authKeys)
      .then(async(res)=>{
          let keys = JSON.parse(res)
          await AsyncStorage.getItem(local)
          .then((result) => {
            let _local = JSON.parse(result)

            updateState({accessKeys:keys , _local: _local})
            callback(keys , _local)
          })
       
      })
     }

     async function getDefaultLoca(){
        await AsyncStorage.getItem(defaultLocal)
        .then((def)=>{
           let defa = JSON.parse(def)
           updateState({_defaultLocal:defa.defaults})
        })
    }

    async function getProducts(keys , loc){
        let cat_id = route.params.id
        let headers ={
            client : keys?.client,
            uid: keys?.uid,
            access_token: keys?.accessToken
          }

    updateState({loading:true})
    await axios.get(`${baseURL}/product/list_products?category_id=${cat_id}`,{
        headers: headers,
        params: {
            locale:loc.toLowerCase()
          },
    })
    .then((response)=>{
        const {data} = response
        updateState({
            productList:data.products,
            productListCopy:data.products,
            loading:false,
        })
    }).catch((error)=>{
        console.log('error ', error);
        updateState({loading:false})
    })

    }



    function SearchBar() {
        return (
            <View style={styles.searchView}>
                <View style={{ ...styles.row, }}>
                    <TextInput
                        placeholder={_defaultLocal.search_here}
                        placeholderTextColor={lightGrey}
                        onChangeText={(text)=> handleSearch(text)}
                        value={searchValue}
                        style={styles.inputField}
                    />
                    <Image
                        source={search}
                        style={styles.filter}
                    />
                </View>
            </View>
        )
    }


const handleSearch=(text)=>{
    let tempList = JSON.parse(JSON.stringify(productList))
   
     const filterData = tempList.filter((obj)=>{
        const regex = new RegExp([text.trim()], 'i')
          if(obj.title.search(regex) >= 0){
            return obj
        }
     })
     
     if(text.trim().length > 0){
     updateState({productList:filterData, searchValue:text})
     }else{
     updateState({productList:productListCopy , searchValue:text})
     }
  }



    function Products(){
        return(
            <>
            {!loading?
           <FlatList
            data={productList}
            keyExtractor={(item)=>item.id}
            renderItem={renderItem}
            numColumns={2}
            columnWrapperStyle={{justifyContent: 'space-between'}}
            style={{marginTop:20}}
            showsVerticalScrollIndicator={false}
            scrollEnabled={true}
            // ListHeaderComponent={SearchBar()}
            />:
            <View style={{marginTop:10}}>
              <ActivityIndicator
              color={red}
              size={'large'}
              />
            </View>
    }
            </>
        )
    }


    const renderItem = ({ item }) => {
        return (
            <>
            <Pressable 
            onPress={()=>navigation.navigate('AppStack',{
                screen:'Detail',
                params:item
            })}
            style={styles.cityView}>
                <View style={styles.countryimgStyle}>
                    <Image
                        // source={flowers}
                        source={{uri:`${domain}${item?.attachments[0]?.url}`}}
                        style={{ width: undefined, height: undefined, flex: 1 }} />
                </View>
                <View style={styles.cityNameView}>
                    <Text style={styles.nameStyle}>{
                    item?.title?.length > 10 ?
                    `${item?.title.substring(0,10)}....` : item?.title
                    }</Text>
                </View>
            </Pressable>
            </>
        )
    }
   
  



    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title={route?.params?.title!=null ? `${route?.params?.title}` : ''}
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                
                <View style={{...styles.row,marginTop:15,}}>
                    <Text style={styles.largeText}>{`Search  ${route?.params?.title}`}</Text>
                    <Image
                        source={filter}
                        style={styles.filter}
                    />
                </View>
                  {SearchBar()}
                  {Products()}

            </View>



        </View>
    )
}

export default SearchFlower