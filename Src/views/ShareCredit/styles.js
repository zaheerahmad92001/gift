import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, ferozi, grey, lightGrey, pink, red, white } from '../../constants/colors'
import { big_tiny, medium, semi_bold, tiny } from '../../constants/appFontSize'
import { appMarginHorizontal, large, small, statusBar } from '../../constants/appFontSize'
import { regular } from '../../constants/fontsFamily';



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1
    },
    header: {
        textAlign: 'center',
        fontFamily: medium,
        fontSize: large,
        color: ferozi
    },
    profileView: {
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileOuterView: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor:pink,
        justifyContent: 'center',
        overflow: 'hidden',
        alignItems: 'center'
    },
    imgStyle: {
        width: 100,
        height: 100,
    },

    iconStyle: {
        color: white,
        fontSize: 20,
    },
   
    inputView: {
        // marginTop: hp(5),
    },
    inputContainerStyle: {
        marginTop: hp(2)
    },
    gradientBtnStyle: {
        width: wp(40),
        marginTop: hp(7),
    },
    textStyle:{
        marginTop:hp(3)
    },
    suggested:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:15,
    },
    imgView:{
        width:50,
        height:50,
        overflow:'hidden',
        alignSelf:'flex-start',
    },
    suggestedUserImg:{
        flex:1,
        height:null,
        width:undefined
    },
    userName:{
        color:black,
        fontFamily:regular,
        fontSize:big_tiny
    },
    nameView:{
     marginLeft:15,
    },
    emailStyle:{
        color:grey,
        fontFamily:regular,
        fontSize:tiny
    }



})
export default styles