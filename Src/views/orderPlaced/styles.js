import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { ferozi, red, white } from '../../constants/colors'
import { semi_bold } from '../../constants/fontsFamily'
import { appMarginHorizontal, large, statusBar } from '../../constants/appFontSize'



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginTop: statusBar,
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    headerView: {
        alignSelf: "flex-start",
        marginTop: hp(4),
    },
    header: {
        fontFamily: semi_bold,
        fontSize: large,
        color: ferozi
    },
    confettiStyle: {
        width: 300,
        height: 200,
        marginTop: hp(6),
        alignSelf: 'center',
        resizeMode: 'contain'
    },
    textView: {
        marginTop: hp(5),
    },
    congrats: {
        textAlign: "center",
        fontFamily: semi_bold,
        fontSize: large,
        color: red
    },
    textStyle: {
        textAlign: "center",
    },
    contentView: {
        marginTop: hp(2)
    },

    signUpBtnText: {
        color: ferozi,
        margin: 8
    },
    signUpBtnStyle: {
        marginTop: hp(2),
    },
    gradientBtnStyle: {
        marginTop: hp(10),
    },
})
export default styles