import React, { useEffect ,useReducer } from 'react'
import { View, Text, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import AppButton from '../../components/appButton'
import HeaderText from '../../components/headerText'
import styles from './styles'
import { confetti } from '../../constants/images'
import { linDarkFerozi, linLightFerozi, white } from '../../constants/colors'
import BackButton from '../../components/backButton'
import { defaultLocal } from '../../constants/constantsValues'
import AsyncStorage from '@react-native-async-storage/async-storage'

function OrderPlaced ({ navigation }){

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
          _defaultLocal:'',
        }
    )
    const {
        _defaultLocal,
    } = state

    useEffect(()=>{
        getDefaultLoca()

    },[])

    async function getDefaultLoca(){
        await AsyncStorage.getItem(defaultLocal)
        .then((def)=>{
           let defa = JSON.parse(def)
           updateState({_defaultLocal:defa.defaults})
        })
    }

    return (
        <View style={styles.wraper}>
            <View style={styles.container}>
             <BackButton onPress={() => { navigation.navigate('BottomTabs',{
                 screen:'Home'})}} />

                <KeyboardAwareScrollView>
                    <Image
                        source={confetti}
                        style={styles.confettiStyle}
                    />
                    <View style={styles.textView}>
                        <Text style={styles.congrats}>Congratulations!</Text>
                    </View>
                    <View style={styles.contentView}>
                        <HeaderText
                            text='Your order has been placed.'
                            textStyle={styles.textStyle} />
                        <HeaderText
                            text='You can view all details in'
                            textStyle={styles.textStyle} />
                        <HeaderText
                            text='My Orders page.'
                            textStyle={styles.textStyle} />

                        <AppButton
                            title={_defaultLocal.continue_shopping}
                            onPress={()=>{}}
                           gradientBtnStyle={styles.gradientBtnStyle}
                        />
                          <AppButton
                            title='GO TO MY ORDERS'
                            onPress={() =>{}}
                            btnColor1={linLightFerozi}
                            btnColor2={linDarkFerozi}
                            gradientBtnStyle={styles.signUpBtnStyle}
                        />

                    </View>
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default OrderPlaced
