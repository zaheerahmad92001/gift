import React,{Coponent}from 'react'
import {View, Text, Image}from 'react-native'
import AppButton from '../../components/appButton'
import { linDarkFerozi, linLightFerozi } from '../../constants/colors'
import * as Animatable from 'react-native-animatable';


import {appIcon} from '../../constants/images'
import { styles } from './styles'

 function AuthView({navigation}){

    return(
        <View style={styles.wraper}>
            <View style={styles.content}>
               <Animatable.Image
               animation="zoomInUp"
               easing="ease-in" 
               duration={2000}
               delay={500}
                source={appIcon}
                style={styles.appIconStyle}
               />
               <Animatable.View 
               animation="fadeIn"
               easing="ease-in" 
               duration={3000}
               delay={500}
               style={styles.btnView}>
               <AppButton
               onPress={()=>{navigation.navigate('AuthStack',{
                   screen:'SignUP'
               })}}
               title={'SIGN UP'}
               />
               <AppButton
               onPress={()=>{navigation.navigate('AuthStack',{screen:'Login'})}}
               title={'LOGIN'}
               btnColor1={linLightFerozi}
               btnColor2={linDarkFerozi}
               gradientBtnStyle={styles.gradientBtnStyle}
               />
               </ Animatable.View>
            </View>
        </View>
    )
 }
 export default AuthView