import React,{Component} from 'react'

import { StyleSheet } from 'react-native'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { appBG } from '../../constants/colors'
export const styles = StyleSheet.create({
    wraper:{
        flex:1,
        backgroundColor:appBG
    },
    content:{
        marginTop:hp(15),
    },
    appIconStyle:{
        width:'100%',
        height:250,
        resizeMode:'contain'
    },
    btnView:{
        marginTop:hp(20)
    },
    gradientBtnStyle:{      
      marginTop:hp(3)
    }
})