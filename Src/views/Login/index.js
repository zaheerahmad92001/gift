import React, { useReducer, useRef, useEffect } from 'react'
import { View, Text, Pressable, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import axios from 'axios'
import DialogBox from 'react-native-dialogbox';
import AsyncStorage from '@react-native-async-storage/async-storage';

import AppButton from '../../components/appButton'
import HeaderText from '../../components/headerText'
import styles from './styles'
import BackButton from '../../components/backButton'
import InputField from '../../components/inputField'
import { ValidateEmail } from '../../Utility/RandomFun'
import { red } from '../../constants/colors'
import { baseURL } from '../../Utility'
import { authKeys, defaultLocal, user } from '../../constants/constantsValues'

function Login({ navigation }) {
    // function Login(props) {

    let dialogboxRef = useRef()
    const emailRef = useRef(null)
    const passwordRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            email: undefined,
            emailError: false,
            password: '',
            passwordError: false,
            securePass: true,
            loading: false,
            _defaultLocal: '',

        }
    )
    const {
        email,
        emailError,
        password,
        passwordError,
        securePass,
        loading,
        _defaultLocal,

    } = state

    useEffect(() => {
        getDefaultLoca()
    },[])


    async function getDefaultLoca() {
        await AsyncStorage.getItem(defaultLocal)
            .then((def) => {
                let defa = JSON.parse(def)
                updateState({ _defaultLocal: defa.defaults })
            })
    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2
            ],
            btn: { text: 'OK', callback: () => { } },
        })

    }



    function hideShowPassword() {
        updateState({ securePass: !securePass })
    }

    const formValidation = () => {
        let error = false
        if (!ValidateEmail(email)) {
            updateState({ emailError: true })
            error = true
        }
        if (password?.trim().length < 6) {
            updateState({ passwordError: true })
            error = true
        }
        return error
    }

    function handleLogin() {
        let validate = formValidation()
        if (!validate) {
            updateState({ loading: true })
            let _data = {
                email: email.toLowerCase(),
                password: password,
            }
            axios.post(`${baseURL}/users/sign_in.json`, _data).then(async (response) => {
                const { data } = response
                if (data) {
                    stopIndicator()
                    let keys = {
                        client: response.headers.client,
                        accessToken: response.headers["access-token"],
                        uid: response.headers.uid
                    }
                    let headers = {
                        client: response.headers.client,
                        uid: response.headers.uid,
                        access_token: response.headers["access-token"]
                    }
                    // get User information
                    axios.get(`${baseURL}/user`, { headers: headers }).then(async (result) => {
                        let USER = result.data.user
                        await AsyncStorage.setItem(user, JSON.stringify(USER))

                    })

                    await AsyncStorage.setItem(authKeys, JSON.stringify(keys))
                    navigation.pop()
                    // navigation.navigate('BottomTabs')
                }
            }).catch((error) => {
                stopIndicator()
                alertMsg('Please check your email and password')
                console.log('error', error);
            })
        }
    }

    function stopIndicator() {
        updateState({ loading: false })
    }

    return (
        <View style={styles.wraper}>
            <View style={styles.container}>
                {/* <BackButton onPress={props.openLogin} /> */}
                <BackButton onPress={() => { navigation.pop() }} />
                <KeyboardAwareScrollView>
                    <View style={styles.headerView}>
                        <Text style={styles.header}>{_defaultLocal.login}</Text>
                    </View>
                    <View style={styles.textView}>
                        <HeaderText
                            text={_defaultLocal.login_in_to_your_account}
                            textStyle={styles.textStyle} />
                    </View>
                    <View style={styles.contentView}>
                        <InputField
                            placeholder={_defaultLocal.email}
                            onChangeText={(text) => { updateState({ email: text, emailError: false }) }}
                            value={email}
                            keyboardType={'email-address'}
                            ref={emailRef}
                            onSubmitEditing={() => passwordRef?.current?.focus()}
                            blurOnSubmit={false}
                            error={emailError}
                        />
                        {emailError &&
                            <Text style={styles.errorText}>{'valid email required'}</Text>
                        }
                        <InputField
                            placeholder={_defaultLocal.password}
                            onChangeText={(text) => { updateState({ password: text, passwordError: false }) }}
                            value={password}
                            keyboardType={'default'}
                            secureTextEntry={securePass}
                            hideShowPassword={hideShowPassword}
                            passIconName={securePass ? 'eye-with-line' : 'eye'}
                            passIconType={'entypo'}
                            ref={passwordRef}
                            blurOnSubmit={false}
                            returnKeyType={'done'}
                            inputContainerStyle={styles.inputContainerStyle}
                            error={passwordError}
                        />
                        {passwordError &&
                            <Text style={styles.errorText}>{'valid password required'}</Text>
                        }

                        {/* <Pressable
                            onPress={() => navigation.navigate('ForgotPassword')}>
                            <Text style={styles.forgotPass}>Forgot Password?</Text>
                        </Pressable> */}
                        {loading ?
                            <View style={styles.indicator}>
                                <ActivityIndicator
                                    color={red}
                                    size={'small'}
                                />
                            </View>
                            :
                            <AppButton
                                title={_defaultLocal.login}
                                onPress={() => handleLogin()}
                                gradientBtnStyle={styles.gradientBtnStyle}
                            />
                        }
                        <View style={styles.sendAgainView}>
                            <Text style={styles.textStyle}>{_defaultLocal.do_not_have_an_account}</Text>
                            <Pressable
                                onPress={
                                    () => {
                                        navigation.navigate('AuthStack', {
                                            screen: 'SignUP'
                                        })
                                    }
                                }>
                                <Text style={styles.sendText}>{_defaultLocal.sign_up}</Text>
                            </Pressable>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
            <DialogBox ref={dialogboxRef} />
        </View>
    )

}
export default Login

