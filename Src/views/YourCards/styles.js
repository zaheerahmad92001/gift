import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, white, red, dark_offwhite, } from '../../constants/colors';
import { medium, regular, semi_bold } from '../../constants/fontsFamily'
import { appMarginHorizontal, large, mediumSize, small, tiny } from '../../constants/appFontSize'



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginTop: hp(3),
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    signUpBtnStyle: {
        width: wp(40),
        marginTop: hp(5),
        borderColor: red,
        borderWidth: 2,
    },
    signUpBtnText: {
        color: red,
        margin: 8
    },
    bottomBtn: {
        position: 'absolute',
        bottom: 0,
        marginBottom: hp(4),
        alignSelf: 'flex-end',
        right: 30,
        zIndex:1,
    },
    floatBtn: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: red,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconStyle: {
        color: white,
        fontSize: 25,
    },

    modalinnerView: {
        backgroundColor: white,
        borderRadius: 20,
        width: wp(90),
        //   width:hp(80)
    },
    blueBGImg: {
        width:80,
        height: 80,
        marginTop: -40,
        alignSelf:'center',
        resizeMode: 'contain'
    },
    middleText: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    largeText: {
        color:red,
        marginTop:hp(1),
        fontFamily:regular,
        fontSize: large,
    },
    modalConten: {
        marginHorizontal: wp(7),
        marginBottom: hp(2),
        marginTop: hp(2),
    },
    mediumText: {
        color: black,
        fontSize: mediumSize,
        fontFamily: regular,
        textAlign: 'center',
    },
    inputContainerStyle: {
        marginTop: 10,
        backgroundColor:dark_offwhite,
        paddingLeft: 10,
        borderColor: 'transparent',
        borderBottomWidth: 0,

    },
    maskedView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    maskedInput: {
        backgroundColor:dark_offwhite,
        marginTop: 10,
        paddingLeft: 10,
        fontSize: 14,
        paddingVertical: 10,
        borderColor: 'transparent',
        borderBottomWidth: 0,
        width: wp(23)

    },
    validText: {
        color: black,
        fontSize: tiny,
        fontFamily: regular,
        marginTop: 10,

    },
    slashText: {
        fontSize: 15,
        fontFamily: medium,
        color: black,
        marginTop: 5,

    },
    gradientBtnCheckOut: {
        width: wp(40),
        marginBottom: hp(3),
        marginTop: hp(1),
    },
    gradientBtnStyle: {
        width: wp(70),
    },

})
export default styles