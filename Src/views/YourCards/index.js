import React, { useReducer, useRef } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from "react-native-modal";
import { MaskedTextInput } from "react-native-mask-text";


import AppHeader from '../../components/appHeader'
import AppButton from '../../components/appButton'
import { popup_card } from '../../constants/images';
import { lightBlack, linDarkFerozi, linLightFerozi, white } from '../../constants/colors';
import styles from './styles'
import _Card from '../../components/Card';
import { Icon } from 'react-native-elements';
import InputField from '../../components/inputField'

function YourCards ({ navigation }){

    const nameRef = useRef(null)
    const cardRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            viaCard: true,
            name: '',
            cardNum: '',
            isVisible:false,
        }
    )
    const { isVisible, name, cardNum } = state

    function addNewCard() {
        return (
            <Modal
                isVisible={isVisible}
                avoidKeyboard={true}
                useNativeDriver={true}
                onBackButtonPress={() => updateState({ isVisible: false })}
                onBackdropPress={() => updateState({ isVisible: false })}
            >

                <View style={styles.modalinnerView}>
                    <Image
                        source={popup_card}
                        style={styles.blueBGImg}
                    />
                    <View style={styles.middleText}>
                        <Text style={styles.largeText}>Add New Card</Text>
                    </View>
                    <View style={styles.modalConten}>
                        <Text style={styles.mediumText}>Please enter your Card Details</Text>
                        <InputField
                            placeholder='Card holder name'
                            placeholderTextColor={lightBlack}
                            onChangeText={(text) => { updateState({ name: text }) }}
                            value={name}
                            keyboardType={'default'}
                            ref={nameRef}
                            inputContainerStyle={{ ...styles.inputContainerStyle, marginTop: '6%' }}
                            onSubmitEditing={() => cardRef?.current?.focus()}
                            blurOnSubmit={false}
                        />
                        <InputField
                            placeholder='Number of card'
                            placeholderTextColor={lightBlack}
                            onChangeText={(text) => { updateState({ name: text }) }}
                            value={cardNum}
                            keyboardType={'default'}
                            ref={cardRef}
                            inputContainerStyle={{ ...styles.inputContainerStyle }}
                            blurOnSubmit={false}
                        />
                        <Text style={styles.validText}>VALID THRU</Text>
                        <View style={styles.maskedView}>
                            <MaskedTextInput
                                mask={'12'}
                                onChangeText={(text, rawText) => {
                                    console.log(text);
                                    console.log(rawText);
                                }}
                            placeholder='MM'
                            placeholderTextColor={lightBlack}
                                style={styles.maskedInput}
                            />
                            <MaskedTextInput
                                mask={'12'}
                                onChangeText={(text, rawText) => {
                                    console.log(text);
                                    console.log(rawText);
                                }}
                                placeholderTextColor={lightBlack}
                                placeholder='YY'
                                style={styles.maskedInput}
                            />

                            <MaskedTextInput
                                mask={'123'}
                                onChangeText={(text, rawText) => {
                                    console.log(text);
                                    console.log(rawText);
                                }}
                               placeholderTextColor={lightBlack}
                                placeholder='CVV'
                                style={styles.maskedInput}
                            />
                        </View>
                    </View>
                  
                    <AppButton
                        title='Save'
                        btnColor1={linLightFerozi}
                        btnColor2={linDarkFerozi}
                        onPress={() => updateState({ isVisible: !isVisible })}
                        gradientBtnStyle={styles.gradientBtnCheckOut}
                    />
                 
                </View>


            </Modal>
        )
    }




    return (
        <View style={styles.wraper}>
          <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title='Your Cards'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    <_Card />
                    <AppButton
                        title='Pay'
                        btnColor1={white}
                        btnColor2={white}
                        onPress={()=>{navigation.navigate('OrderPlaced')}}
                        btnText={styles.signUpBtnText}
                        gradientBtnStyle={styles.signUpBtnStyle}
                    />
                </KeyboardAwareScrollView>

            </View>
            <TouchableOpacity
                onPress={() => updateState({ isVisible:!isVisible })}
                style={styles.bottomBtn}>
                <View style={styles.floatBtn}>
                    <Icon
                        name='plus'
                        type='antdesign'
                        iconStyle={styles.iconStyle}
                    />
                </View>
            </TouchableOpacity>
            {addNewCard()} 

        </View>
    )
}

export default YourCards