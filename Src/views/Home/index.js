import React, { useReducer, useRef, useEffect } from 'react'
import { View, Image, Text, FlatList, TextInput, ActivityIndicator, Pressable } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import axios from 'axios'
import Swiper from 'react-native-swiper'
import RNLocalize from "react-native-localize";
import { Flag } from 'react-native-svg-flagkit'



import { authKeys, local  , defaultLocal} from '../../constants/constantsValues'
import { baseURL, domain } from '../../Utility'
import AppHeader from '../../components/appHeader'
import { baloons } from '../../constants/images';
import styles from './styles'
import { filter, search, home_slider } from '../../constants/images'
import { lightGrey, red } from '../../constants/colors';

const imgHeight = 180


function Home({ navigation }) {

    const nameRef = useRef(null)
    const cardRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            accessKeys: undefined,
            catList: undefined,
            catListCopy: undefined,
            loading: false,
            adsLoading: false,
            adsList: undefined,
            searchValue: undefined,
            country: '',
            _local: '',
            _defaultLocal:'',
        }
    )
    const {
        searchValue,
        accessKeys,
        catList,
        catListCopy,
        loading,
        adsLoading,
        adsList,
        country,
        _local,
        _defaultLocal,
    } = state

    useEffect(() => {
        getKeys(getCategories, getAds , getDefaultLoca)
        getCountry()

    }, [])

    async function getKeys(callback, callAds) {
        await AsyncStorage.getItem(authKeys)
            .then(async (res) => {
                let keys = JSON.parse(res)

                await AsyncStorage.getItem(local)
                    .then((result) => {
                        let _local = JSON.parse(result) ? JSON.parse(result) : 'EN'
                        updateState({ accessKeys: keys, _local: _local })
                        callback(keys, _local)
                        callAds(keys, _local)
                        getDefaultLoca(_local)
                    })
                //  updateState({accessKeys:keys})
                //  callback(keys)
                //  callAds(keys)
            })
    }

    async function getCategories(keys , loc) {
        let headers = {
            client: keys?.client,
            uid: keys?.uid,
            access_token: keys?.accessToken
        }

        updateState({ loading: true })
        await axios.get(`${baseURL}/product/list_categories`, { 
            headers: headers,
            params: {
                locale:loc.toLowerCase()
              },
         })
            .then((response) => {
                const { data } = response
                console.log('here is products response', data);
                if (data) {
                    updateState({
                        catList: data?.categories,
                        catListCopy: data?.categories,
                        loading: false
                    })
                }
            }).catch((error) => {
                console.log('error ', error);
                updateState({ loading: false })
            })
    }


    async function getAds(keys , loc) {
        console.log('local is' , loc);
        let headers = {
            client: keys?.client,
            uid: keys?.uid,
            access_token: keys?.accessToken
        }

        updateState({ adsLoading: true })
        await axios.get(`${baseURL}/ad/fetch_ads`, { 
            headers: headers,
            params: {
                locale:loc.toLowerCase()
              },

         })
            .then((response) => {
                console.log('ads response', response);
                const { data } = response
                if (data?.api_status) {
                    updateState({
                        adsList: data.ads,
                        adsLoading: false
                    })
                }

            }).catch((error) => {
                console.log('error ', error);
                updateState({ adsLoading: false })
            })

    }

    async function getCountry() {
        let country = await RNLocalize.getCountry()
        // console.log('country', country);
        updateState({ country: country })
    }


    function SearchBar() {
        return (
            <View style={styles.searchView}>
                <View style={{ ...styles.row, }}>
                    <TextInput
                        placeholder={_defaultLocal.search_here}
                        placeholderTextColor={lightGrey}
                        onChangeText={(text) => handleSearch(text)}
                        value={searchValue}
                        style={styles.inputField}
                    />
                    <Image
                        source={search}
                        style={styles.filter}
                    />
                </View>
            </View>
        )
    }

    const handleSearch = (text) => {
        let tempList = JSON.parse(JSON.stringify(catList))

        const filterData = tempList.filter((obj) => {
            const regex = new RegExp([text.trim()], 'i')
            if (obj.title.search(regex) >= 0) {
                return obj
            }
        })

        if (text.trim().length > 0) {
            updateState({ catList: filterData, searchValue: text })
        } else {
            updateState({ catList: catListCopy, searchValue: text })
        }
    }

    function SwiperView() {
        return (
            <View style={{ marginTop: 10 }}>
                {!adsLoading && adsList?.length > 0 ?

                    <Swiper
                        loop={true}
                        autoplay={true}
                        autoplayTimeout={5}
                        autoplayDirection={true}
                        // dot={<View style={styles.dotStyle} />}
                        // activeDot={<View style={styles.activeDotStyle} />}
                        style={{ height: (imgHeight), }}
                        containerStyle={{ height: imgHeight }}
                    >
                        {adsList.map((item, index) => {
                            return (
                                <View style={{ height: imgHeight }}>
                                    <Image
                                        //  source={home_slider}
                                        source={{ uri: `${item?.value}` }}
                                        style={{ height: imgHeight, width: '100%', }}
                                    />
                                </View>
                            )
                        })}
                    </Swiper>
                    : !adsLoading && adsList?.length == 0 ?
                        <View style={styles.noAds}><Text>No ads</Text></View>
                        :
                        <View style={{ marginBottom: hp(10), marginTop: 10, }}><ActivityIndicator color={red} size={'large'} /></View>
                }
            </View>
        )
    }

    function Products() {
        return (
            <>
                {!loading ?
                    <FlatList
                        data={catList}
                        keyExtractor={(item) => item.id}
                        renderItem={renderItem}
                        numColumns={2}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        style={{ marginTop: 20 }}
                        extraData={catList}
                        showsVerticalScrollIndicator={false}
                    />
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator
                            color={red}
                            size={'large'}
                        />
                    </View>
                }
            </>
        )
    }

    const renderItem = ({ item, index }) => {
        return (
            <Pressable
                onPress={() => navigation.navigate('AppStack', {
                    screen: 'SearchFlower',
                    params: item
                })}
                style={styles.cityView}>
                <View style={styles.countryimgStyle}>
                    <Image
                        // source={baloons}
                        source={{ uri: `${domain}${item?.attachment.url}` }}
                        style={{ width: undefined, height: undefined, flex: 1 }} />
                </View>
                <View style={styles.cityNameView}>
                    <Text style={styles.nameStyle}>{item?.title}</Text>
                </View>
            </Pressable>
        )
    }

    async function changeLocal(val) {
        updateState({_local:val})
        await AsyncStorage.setItem(local ,JSON.stringify (val) )
        getAds(accessKeys , val)
        getCategories(accessKeys , val)
        getDefaultLoca(val)

    }

    async function getDefaultLoca(loc){
        await axios.get(`${baseURL}/default_locales`, { 
            // headers: headers ,
            params: {
                locale:loc.toLowerCase()
              },
        })
            .then(async(response) => {
                const { data } = response
                console.log('default local response', data);
                await AsyncStorage.setItem(defaultLocal, JSON.stringify(data))
                if (data?.defaults) {
                    updateState({
                        _defaultLocal: data.defaults,
                    })
                }

            }).catch((error) => {
                console.log('error ', error);
                updateState({ adsLoading: false })
            })

    }



    return (
        <View style={styles.wraper}>
            <AppHeader
                dropdown={country}
                // data={data}
                selectedValue={''}
                placeholder={false}
                title={_defaultLocal.home?.toUpperCase()}
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                >
                 {_local =='EN'?
                    <Pressable
                        onPress={()=>changeLocal('AR')}
                        style={styles.localView}>
                        <Text style={styles.standerText}>{'AR'}</Text>
                    </Pressable> :

                    <Pressable
                        onPress={()=>changeLocal('EN')}
                        style={styles.localView}>
                        <Text style={styles.standerText}>{'EN'}</Text>
                    </Pressable>
                 }


                    <View style={{ ...styles.row, marginTop: 15, }}>
                        <Text style={styles.largeText}>{_defaultLocal.search_gifts}</Text>
                        <Image
                            source={filter}
                            style={styles.filter}
                        />
                    </View>
                    {SearchBar()}
                    {SwiperView()}
                    {Products()}
                </KeyboardAwareScrollView>

            </View>



        </View>
    )
}

export default Home