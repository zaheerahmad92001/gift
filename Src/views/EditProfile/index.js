import React, { useReducer,useEffect, useRef } from 'react'
import { View, Pressable, Image ,Text ,ActivityIndicator, Platform } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {launchImageLibrary ,launchCamera } from "react-native-image-picker"
import DialogBox from 'react-native-dialogbox';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios'


import styles from './styles'
import {authKeys, defaultLocal, user} from '../../constants/constantsValues'
import { Icon , Overlay } from 'react-native-elements'
import { red } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import{options}from '../../constants/constantsValues'
import InputField from '../../components/inputField'
import SIcon from 'react-native-vector-icons/SimpleLineIcons';
import AppButton from '../../components/appButton'
import { baseURL } from '../../Utility';


function EditProfile({ navigation }){

    let dialogboxRef = useRef()
    const nameRef = useRef(null)
    const emailRef = useRef(null)
    const contactRef = useRef(null)
    const addressRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            filePath: '',
            name: undefined,
            nameError:false,
            email: undefined,
            contact: undefined,
            address:undefined,
            isVisible: false,
            User:'',
            accessKeys:'',
            sending:false,
           _defaultLocal:'',


        }
    )
    const { 
        filePath, name, 
        email, contact , 
        address ,isVisible,
        sending , accessKeys,
        nameError,
       _defaultLocal,
 
      } = state


    useEffect(()=>{
        const unsubscribe = navigation.addListener('focus', () => {
            getUser()
            getKeys()
         getDefaultLoca()

           });
      return unsubscribe;
      },[])

     
      async function getDefaultLoca(){
        await AsyncStorage.getItem(defaultLocal)
        .then((def)=>{
           let defa = JSON.parse(def)
           updateState({_defaultLocal:defa.defaults})
        })
    }

 async function getUser(){
  await AsyncStorage.getItem(user).then((res)=>{
      let user = JSON.parse(res)
      updateState({
          User:user,
          name:user.first_name,
          email:user.email,
          address:user.address,
          contact:user.contact_number
        })
  })
 }

 async function getKeys(){
    await AsyncStorage.getItem(authKeys)
  .then((res)=>{
      let keys = JSON.parse(res)
     updateState({accessKeys:keys})
  })
 }

async function updateProfile(){
    let error = false
    let headers ={
        client : accessKeys?.client,
        uid: accessKeys?.uid,
        access_token: accessKeys?.accessToken,
      }
    
      let _data={}
    // let formData = new FormData()

    if(name.trim().length >0){
       _data.first_name = name 

    }else{
      updateState({nameError:true})
      error = true
    }
    _data.address = address,
    _data.contact_number= contact

    if(filePath){
    }
 
if(!error){
  updateState({sending:true})
  await axios.put(`${baseURL}/user`,_data,{
    headers: headers,
}).then(async(result)=>{
    const {data} = result
    // console.log('data?.user.api_status',data);
    if(data?.user.api_status){
        updateState({sending:false})
        let USER = data?.user
        await AsyncStorage.setItem(user,JSON.stringify(USER))
    }else{
        alertMsg('Something went wrong')
        updateState({sending:false})
    }
  }).catch((error)=>{
      console.log('error',error);
  })
}

 }
 function alertMsg(msg1, msg2) {
    dialogboxRef.current.tip({
        title: 'Message',
        content: [
            msg1,
            msg2
        ],
        btn: { text: 'OK', callback: () => { } },
    })

}



    function opneImagePicker() {
 
        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                updateState({
                    filePath: response.assets[0],
                });
            }
        });
    }

    function opneCamera() {
 
        launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                updateState({
                    filePath: response.assets[0],
                });
            }
        });
    }

    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.profileOuterView}>
                    {!filePath ?
                        <SIcon name="user" style={{ fontSize: 40, color:red }} />
                        :
                        <View style={styles.imgStyle}>
                            <Image
                                source={{ uri: filePath.uri }}
                                style={{ width: undefined, height: undefined, flex: 1 }} />
                        </View>
                    }
                </View>
                <Pressable
                    // onPress={opneImagePicker}
                    onPress={()=>updateState({isVisible:true})}
                    style={styles.editView}>
                    <Icon
                        name={'edit'}
                        type={'entypo'}
                        iconStyle={styles.iconStyle}
                        tvParallaxProperties={undefined}
                    />
                </Pressable>
            </View>
        )
    }

    function InFoUI() {
        return (
            <View style={styles.inputView}>
                <InputField
                    placeholder={_defaultLocal.name}
                    onChangeText={(text) => { updateState({ name: text,nameError:false }) }}
                    value={name}
                    keyboardType={'default'}
                    ref={nameRef}
                    onSubmitEditing={() => emailRef?.current?.focus()}
                    blurOnSubmit={false}
                    error={nameError}

                />
                <InputField
                    placeholder={_defaultLocal.email}
                    onChangeText={(text) => { updateState({ email: text }) }}
                    value={email}
                    editable={false}
                    keyboardType={'email-address'}
                    inputContainerStyle={styles.inputContainerStyle}
                    ref={emailRef}
                    onSubmitEditing={() => addressRef?.current?.focus()}
                    blurOnSubmit={false}
                />
                 <InputField
                    placeholder={_defaultLocal.address}
                    onChangeText={(text) => { updateState({ address: text }) }}
                    value={address}
                    keyboardType={'default'}
                    inputContainerStyle={styles.inputContainerStyle}
                    ref={addressRef}
                    onSubmitEditing={() => contactRef?.current?.focus()}
                    blurOnSubmit={false}
                />
                <InputField
                    placeholder='Contact Number'
                    onChangeText={(text) => { updateState({ contact: text }) }}
                    value={contact}
                    keyboardType={'number-pad'}
                    inputContainerStyle={styles.inputContainerStyle}
                    ref={contactRef}
                    blurOnSubmit={false}
                />
                {sending ?
                <View style={styles.indicator}>
                  <ActivityIndicator  color={red} size={'small'}/>
                </View>
                :
                <AppButton
                    title='SAVE CHANGES'
                    onPress={() => {updateProfile()}}
                    disabled={nameError}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
              }       
            </View>
        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader 
             leftPress={()=>navigation.pop()}
             iconName='chevron-back'
             iconType='ionicon'
             title={_defaultLocal.edit_profile} 
             rightIconName={'question'}
             rightIconType={'antdesign'}
             rightPress={()=>{}}
             />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {InFoUI()}
                </KeyboardAwareScrollView>
            </View>


            <Overlay
                isVisible={isVisible}
                avoidKeyboard={true}
                useNativeDriver={true}
                onBackButtonPress={() => updateState({ isVisible: false })}
                onBackdropPress={() => updateState({ isVisible: false })}
                overlayStyle={styles.modalinnerView}>
                    <View style={styles.modalConten}>
                        <Text style={styles.heading}>Select Image</Text>
                        
                        <Pressable
                         onPress={()=>{
                            updateState({isVisible:false})
                            setTimeout(() => {
                              opneCamera()   
                            },600);
                        }}
                         style={{marginTop:10,}}
                        >
                         <Text style={styles.btnText}>Launch Camera</Text>
                       </Pressable>
                       <Pressable
                        onPress={()=>{
                            updateState({isVisible:false})
                            setTimeout(() => {
                              opneImagePicker()   
                            },600);
                        }}
                        style={{marginTop:10,}}
                       >
                         <Text style={styles.btnText}>Select from Gallery</Text>
                       </Pressable>

                    </View>
                </Overlay>
            <DialogBox ref={dialogboxRef} />

        </View>
    )
}

export default EditProfile
