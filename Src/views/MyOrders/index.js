import React, { useEffect, useReducer } from 'react'
import { View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles'
import AppHeader from '../../components/appHeader'
import OrderCard from '../../components/orderCard'
import AppButton from '../../components/appButton'
import { linDarkFerozi, linLightFerozi } from '../../constants/colors'
import { defaultLocal } from '../../constants/constantsValues'
import AsyncStorage from '@react-native-async-storage/async-storage'


function MyOrders ({ navigation }) {
    

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
          _defaultLocal:'',

        }
    )
    const {
      _defaultLocal,

    } = state

    useEffect(()=>{
      getDefaultLoca()

    },[])

    async function getDefaultLoca(){
      await AsyncStorage.getItem(defaultLocal)
      .then((def)=>{
         let defa = JSON.parse(def)
         updateState({_defaultLocal:defa.defaults})
      })
  }


    return (
        <View style={styles.wraper}>
            <AppHeader 
             title={_defaultLocal.my_orders}
             iconName='chevron-back'
             iconType='ionicon'
             leftPress={()=>{navigation.goBack()}}
             rightIconName={'question'}
             rightIconType={'antdesign'}
             rightPress={()=>{}}
             />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                  <OrderCard
                //    onPress={()=>navigation.navigate('MyCart')}
                  />
                </KeyboardAwareScrollView>
                <View style={{
                      position:'absolute',
                      bottom:30,
                      alignSelf:'center'
                  }}>
                  <AppButton
                    title={_defaultLocal.continue_shopping}
                    onPress={()=>{}}
                    btnColor1={linLightFerozi}
                    btnColor2={linDarkFerozi}
                  />
                  </View>
            </View>
        </View>
    )
}

export default MyOrders
