import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, Image, Pressable } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles'
import { linDarkFerozi, linLightFerozi,grey } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import { appIcon, gift , gift_recv, gift_send, gift_money, gift_credit } from '../../constants/images'
import AppButton from '../../components/appButton'
import HeaderText from '../../components/headerText'
import { defaultLocal } from '../../constants/constantsValues'
import AsyncStorage from '@react-native-async-storage/async-storage'


function GiftReceived({ navigation }) {

    const emailRef = useRef(null)
    const nameRef = useRef(null)
    const reviewRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            filePath: '',
            name:undefined,
            email:undefined,
            reviews:undefined,
           _defaultLocal:'',

        }
    )
    const { 
        filePath,
         name , 
         email, 
         reviews,
        _defaultLocal,

    } = state

    useEffect(()=>{
        getDefaultLoca()

    },[])

    async function getDefaultLoca(){
        await AsyncStorage.getItem(defaultLocal)
        .then((def)=>{
           let defa = JSON.parse(def)
           updateState({_defaultLocal:defa.defaults})
        })
    }


    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.imgStyle}>
                    <Image
                        source={appIcon}
                        style={{ width: undefined, height: undefined, flex: 1 }} />
                </View>
            </View>
        )
    }

    function InFoUI() {
        return (
            <View style={styles.inputView}>
             <HeaderText
             text={_defaultLocal.you_have_received_following_gift}
             />
             <View style={styles.row}>
                 <View style={styles.giftImg}>
                     <Image
                     source={gift}
                     style={styles.img}
                     />
                 </View>
                 <View style={styles.giftContent}>
                     <HeaderText
                     text={'Fresh Flowers Bouqet'}
                     textStyle={styles.giftName}
                     />
                      <HeaderText
                     text={'$300'}
                     textStyle={styles.giftPrice}
                     />
                 </View>
             </View>

            </View>
        )
    }

    function GiftTabs() {
        return (
            <View>
                <Pressable
                    onPress={() =>{}}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={gift_recv} style={styles.giftImgStyle} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.receive_gift}</Text>
                        </View>
                    </View>
                </Pressable>

                <Pressable
                    onPress={() => {}}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={gift_send} style={styles.giftImgStyle} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.forward_gift}</Text>
                        </View>
                    </View>
                </Pressable>

                <Pressable
                    onPress={() =>{}}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={gift_money} style={styles.giftImgStyle} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.receive_in_money}</Text>
                        </View>
                    </View>
                </Pressable>

                <Pressable
                    onPress={() =>{}}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={gift_credit} style={styles.giftImgStyle} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.go_to_my_cart}</Text>
                        </View>
                    </View>
                </Pressable>

            </View>

        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title='Gifts'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {InFoUI()}
                    {GiftTabs()}
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default GiftReceived
