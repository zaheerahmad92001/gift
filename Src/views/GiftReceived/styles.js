import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, ferozi, pink, red, white } from '../../constants/colors'
import { small, tiny } from '../../constants/appFontSize'
import { appMarginHorizontal } from '../../constants/appFontSize'
import { regular } from '../../constants/fontsFamily';



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1
    },
   
    profileView: {
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
   
    imgStyle: {
        width: 130,
        height: 130,
        resizeMode:'contain',
        overflow:'hidden',
    },
    gradientBtnStyle: {
        width: wp(40),
        marginTop: hp(7),
    },
    giftImg:{
        width:60,
        height:60,
        overflow:'hidden',
        borderRadius:10,
    },
    img:{
        width:undefined,
        height:undefined,
        flex:1,
    },
    giftName:{
        color:black,
        fontSize:tiny,
        fontFamily:regular,   
    },
    row:{
        flexDirection:'row',
        marginTop:hp(2),
        alignItems:'center',
        justifyContent:'center'
    },

    giftContent:{
        marginLeft:10,
    },

    giftPrice:{
        color:red,
        fontFamily:regular,
        fontSize:small,
        alignSelf:'flex-start'
    },
    buttoncardView: {
        shadowColor:ferozi,
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        marginTop: hp(3),
        paddingTop: 12,
        paddingBottom: 12,
    },
    buttonCardContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: wp(2),
    },
    leftSide: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: wp(2)
    },
    giftImgStyle:{
        width: 40, 
        height: 40, 
        resizeMode: 'contain' 
    },
    buttonTitle:{
        fontSize:small,
        fontFamily:regular,
        color:black,
        marginLeft:15,
    }

    
    

})
export default styles