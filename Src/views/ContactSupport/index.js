import React, { useReducer, useRef } from 'react'
import { View, TextInput, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles'
import { linDarkFerozi, linLightFerozi,grey } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import { support } from '../../constants/images'
import AppButton from '../../components/appButton'


function ContactSupport({ navigation }) {

    const emailRef = useRef(null)
    const nameRef = useRef(null)
    const reviewRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            filePath: '',
            name:undefined,
            email:undefined,
            reviews:undefined,
        }
    )
    const { filePath, name , email, reviews } = state


    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.profileOuterView}>
                    {!filePath ?
                        <Image
                            source={support}
                            resizeMode='contain'
                            style={{ width: 40, height: 40 }}
                        />
                        :
                        <View style={styles.imgStyle}>
                            <Image
                                source={{ uri: filePath.uri }}
                                style={{ width: undefined, height: undefined, flex: 1 }} />
                        </View>
                    }
                </View>
            </View>
        )
    }

    function InFoUI() {
        return (
            <View style={styles.inputView}>
             <View style={styles.textView}>
                  <TextInput
                  placeholder='Name'
                  placeholderTextColor={grey}
                  onChangeText={(text)=>updateState({name:text})}
                  value={name}
                  onSubmitEditing={()=>emailRef?.current?.focus()}
                  blurOnSubmit={false}
                  ref={nameRef}
                  style={styles.textInput}
                  />
                 </View>
                 <View style={styles.textView}>
                  <TextInput
                  placeholder='Email'
                  placeholderTextColor={grey}
                  onChangeText={(text)=>updateState({email:text})}
                  value={email}
                  onSubmitEditing={()=>reviewRef?.current?.focus()}
                  blurOnSubmit={false}
                  ref={emailRef}
                  style={styles.textInput}
                  />
                 </View>

                 <View style={styles.reviewsBox}>
                  <TextInput
                  placeholder='Tell us how we can help you?'
                  placeholderTextColor={grey}
                  onChangeText={(text)=>updateState({reviews:text})}
                  value={reviews}
                  blurOnSubmit={false}
                  ref={reviewRef}
                  multiline={true}
                  style={styles.textInput}
                  />
                 </View>
                <AppButton
                    title='SUBMIT'
                    onPress={() => { }}
                    btnColor1={linLightFerozi}
                    btnColor2={linDarkFerozi}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
            </View>
        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title='Contact Support'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {InFoUI()}
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default ContactSupport
