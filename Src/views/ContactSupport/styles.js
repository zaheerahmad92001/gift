import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, pink, white } from '../../constants/colors'
import { small } from '../../constants/appFontSize'
import { appMarginHorizontal } from '../../constants/appFontSize'
import { regular } from '../../constants/fontsFamily';



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1
    },
   
    profileView: {
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileOuterView: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor:pink,
        justifyContent: 'center',
        overflow: 'hidden',
        alignItems: 'center'
    },
    imgStyle: {
        width: 100,
        height: 100,
    },

    gradientBtnStyle: {
        width: wp(40),
        marginTop: hp(7),
    },

    textView: {
        shadowColor:'#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        marginTop:hp(3),
        width:wp(87),
        alignSelf:'center',
        borderRadius:5,
    },
    textInput:{
        paddingLeft:10,
        flex:1,
        paddingVertical:12,
        fontFamily:regular,
        fontSize:small,
        color:black,
    },
    reviewsBox:{
        height:hp(13),
        textAlignVertical:'top',
        justifyContent:'flex-start',
        shadowColor:'#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        marginTop:hp(3),
        width:wp(88),
        borderRadius:5,
    }

})
export default styles