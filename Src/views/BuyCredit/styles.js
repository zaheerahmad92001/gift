import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, ferozi, pink, white } from '../../constants/colors'
import { mediumSize, semi_bold, tiny } from '../../constants/appFontSize'
import { appMarginHorizontal } from '../../constants/appFontSize'
import { regular } from '../../constants/fontsFamily';



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1
    },
   
    profileView: {
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileOuterView: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor:pink,
        justifyContent: 'center',
        overflow: 'hidden',
        alignItems: 'center'
    },
    imgStyle: {
        width: 100,
        height: 100,
    },

    iconStyle: {
        color:white,
        fontSize:15,
    },
    checkBGColor:{
      width:20,
      height:20,
      borderRadius:20/2,
      backgroundColor:ferozi,
      justifyContent:'center',
      alignSelf:'flex-end',
      alignItems:'center',
      marginRight:5,
    },
    gradientBtnStyle: {
        width: wp(40),
        marginTop: hp(7),
    },
    textStyle:{
        marginTop:hp(3)
    },
   
    enterAmount:{
        alignSelf:'flex-start',
        fontSize:semi_bold,
        fontSize:mediumSize,
        marginTop:hp(2),
    },
    textView: {
        shadowColor:'#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        marginTop:hp(3),
        width:wp(25),
        alignSelf:'center',
        paddingVertical:5,
        borderRadius:10,
        marginHorizontal:5,
    },
    textInput:{
        paddingLeft:10,
        flex:1,
        paddingVertical:12,
    },
    payingImg:{
        width:40,
        height:40,
        resizeMode:'contain',
        alignSelf:'center',
    },
    paymentView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:10,
    },
    methodName:{
        color:black,
        fontSize:tiny,
        fontFamily:regular,
        textAlign:'center',
        marginTop:5,
    }

})
export default styles