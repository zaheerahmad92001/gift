import React, { useReducer, useRef } from 'react'
import { View, Pressable, Image, Text } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles'
import { Icon } from 'react-native-elements'
import { linDarkFerozi, linLightFerozi } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import HeaderText from '../../components/headerText'
import { credit_card, apple_pay, google_pay, buy_credit } from '../../constants/images'
import AppButton from '../../components/appButton'


function BuyCredit({ navigation }) {

    const emailRef = useRef(null)
    const addressRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            filePath: '',
        }
    )
    const { filePath } = state


    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.profileOuterView}>
                    {!filePath ?
                        <Image
                            source={buy_credit}
                            resizeMode='contain'
                            style={{ width: 40, height: 40 }}
                        />
                        :
                        <View style={styles.imgStyle}>
                            <Image
                                source={{ uri: filePath.uri }}
                                style={{ width: undefined, height: undefined, flex: 1 }} />
                        </View>
                    }
                </View>
            </View>
        )
    }

    function InFoUI() {
        return (
            <View style={styles.inputView}>
                <HeaderText
                    text={'Buy Credit using'}
                    textStyle={styles.enterAmount}
                />
                <View style={styles.paymentView}>
                    <Pressable>
                    <View style={styles.textView}>
                        <View style={styles.checkBGColor}>
                            <Icon type={'antdesign'} name={'check'}
                                iconStyle={styles.iconStyle}
                            />
                        </View>
                        <Image
                            source={credit_card}
                            style={styles.payingImg}
                        />
                    </View>
                    <Text style={styles.methodName}>Credit Card</Text>
                    </Pressable>

                    <Pressable>
                    <View style={styles.textView}>
                        <View style={styles.checkBGColor}>
                            <Icon type={'antdesign'} name={'check'}
                                iconStyle={styles.iconStyle}
                            />
                        </View>
                        <Image
                            source={google_pay}
                            style={styles.payingImg}
                        />
                    </View>
                    <Text style={styles.methodName}>Google Pay</Text>
                    </Pressable>

                    <Pressable>
                    <View style={styles.textView}>
                        <View style={styles.checkBGColor}>
                            <Icon type={'antdesign'} name={'check'}
                                iconStyle={styles.iconStyle}
                            />
                        </View>
                        <Image
                            source={apple_pay}
                            style={styles.payingImg}
                        />
                    </View>
                    <Text style={styles.methodName}>Apple Pay</Text>
                    </Pressable>

                </View>
                <AppButton
                    title='BUY'
                    onPress={() => { }}
                    btnColor1={linLightFerozi}
                    btnColor2={linDarkFerozi}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
            </View>
        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title='Buy Credit'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {InFoUI()}
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default BuyCredit
