import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, grey, pink, red, white } from '../../constants/colors'
import { big_tiny, small, tiny } from '../../constants/appFontSize'
import { appMarginHorizontal } from '../../constants/appFontSize'
import { regular } from '../../constants/fontsFamily';



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1
    },
   
    profileView: {
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileOuterView: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor:pink,
        justifyContent: 'center',
        overflow: 'hidden',
        alignItems: 'center'
    },
    imgStyle: {
        width: 100,
        height: 100,
    },

    gradientBtnStyle: {
        width: wp(40),
        marginTop: hp(7),
    },

    suggested:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:15,
    },
    imgView:{
        width:50,
        height:50,
        overflow:'hidden',
        alignSelf:'flex-start',
    },
    suggestedUserImg:{
        flex:1,
        height:null,
        width:undefined
    },
    userName:{
        color:black,
        fontFamily:regular,
        fontSize:big_tiny
    },
    nameView:{
     marginLeft:15,
    },
    emailStyle:{
        color:grey,
        fontFamily:regular,
        fontSize:tiny
    },
    circle:{
        width:20,
        height:20,
        borderRadius:20/2,
        backgroundColor:red,
        alignItems:'center',
        justifyContent:'center',
    },
    outerView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    iconStyle:{
        fontSize:15,
        color:white,
    },
    textStyle:{
        marginTop:hp(3),
        color:red,
    },
})
export default styles