import React, { useReducer, useRef } from 'react'
import { View, Pressable, Image,Text } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styles from './styles'
import { linDarkFerozi, linLightFerozi,grey } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import { sendGift_red,ladyPic } from '../../constants/images'
import AppButton from '../../components/appButton'
import HeaderText from '../../components/headerText'
import InputField from '../../components/inputField'
import { Icon } from 'react-native-elements/dist/icons/Icon'



function ForwardGift({ navigation }) {

    const emailRef = useRef(null)
    const nameRef = useRef(null)
    const reviewRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            email:undefined,

        }
    )
    const {  email,  } = state


    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.profileOuterView}>
                        <Image
                            source={sendGift_red}
                            resizeMode='contain'
                            style={{ width: 40, height: 40 }}
                        />
                </View>
            </View>
        )
    }

    function suggestedUser(){
        return(
            <Pressable>
        <View style={styles.outerView}>
            <View style={styles.suggested}>
           <View style={styles.imgView}>
              <Image source={ladyPic} style={styles.suggestedUserImg}/>
           </View>
           <View style={styles.nameView}>
               <Text style={styles.userName}>Anna Williams</Text>
               <HeaderText text={'anna@email.com'} textStyle={styles.emailStyle}/>
           </View>
            </View>
            <View style={styles.circle}>
                <Icon
                  name='check'
                  type='antdesign'
                  iconStyle={styles.iconStyle}
                />
            </View>
        </View>
           </Pressable>
        )
    }

    function InFoUI() {
        return (
            <View style={styles.inputView}>
             
            <HeaderText text='Please enter email below to'/>
            <HeaderText text='search user for forwarding'/>
                <InputField
                    placeholder='Email'
                    onChangeText={(text) => { updateState({ email: text }) }}
                    value={email}
                    keyboardType={'email-address'}
                    ref={emailRef}
                    onSubmitEditing={()=>passwordRef?.current?.focus()}
                    blurOnSubmit={false}
                 />
                    <HeaderText
                     text={'Suggested User'}
                     textStyle={[styles.textStyle]}
                   />
                 {suggestedUser()}

                <AppButton
                    title='FORWARD'
                    onPress={() => { }}
                    btnColor1={linLightFerozi}
                    btnColor2={linDarkFerozi}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
            </View>
        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title='Contact Support'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {InFoUI()}
                    
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default ForwardGift
