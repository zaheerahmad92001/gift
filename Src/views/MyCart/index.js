import React, { useReducer, useEffect, useRef } from 'react'
import { View, Text, Image, TextInput, FlatList, Pressable } from 'react-native'
import Modal from "react-native-modal";
import { Icon, Overlay } from 'react-native-elements'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import DialogBox from 'react-native-dialogbox';




import styles from './styles'
import AppHeader from '../../components/appHeader'
import AppButton from '../../components/appButton'
import { black, ferozi, grey, linDarkFerozi, linLightFerozi, red, white } from '../../constants/colors'
import Cart from '../../components/myCart'
import HeaderText from '../../components/headerText'
import { heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { medium } from '../../constants/fontsFamily'
import { popup_edit, popup_card, delete_popup } from '../../constants/images'
import { myCart, shopingCard, authKeys, local, defaultLocal } from '../../constants/constantsValues';
import { baseURL } from '../../Utility';


let data = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
]

function MyCart({ navigation, route }) {

    let dialogboxRef = useRef()

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            isVisible: false,
            reviews: undefined,
            showPaymentModal: false,
            viaCard: false,
            ondelivery: true,
            showDeleteModal: false,
            shopingCard: undefined,
            loading: false,
            cartList: '',
            selectedCart: '',
            cartIndex: '',
            accessKeys: '',
            processing: false,
            _local: '',
            _defaultLocal:'',
        }
    )
    const {
        isVisible,
        reviews,
        showPaymentModal,
        viaCard, ondelivery,
        showDeleteModal,
        loading,
        cartList,
        selectedCart,
        cartIndex,
        accessKeys,
        processing,
        _local,
        _defaultLocal,

    } = state


useEffect(()=>{
    getDefaultLoca()
},[])

async function getDefaultLoca(){
    await AsyncStorage.getItem(defaultLocal)
    .then((def)=>{
       let defa = JSON.parse(def)
       console.log('default', defa);
       updateState({_defaultLocal:defa.defaults})
    })
}

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2
            ],
            btn: { text: 'OK', callback: () => { } },
        })

    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            // getShoingCard()
            getKeys(getCart)

        });
        return unsubscribe;
    }, [])


    async function getKeys(callback) {
        await AsyncStorage.getItem(authKeys)
            .then(async (res) => {
                let keys = JSON.parse(res)
                await AsyncStorage.getItem(local)
                    .then((result) => {
                        let _local = JSON.parse(result)
                        updateState({ accessKeys: keys, _local: _local })
                        callback(keys, _local)
                    })

            })
    }

    // async function getShoingCard() {
    //     await AsyncStorage.getItem(myCart).then((res) => {
    //         const cart = JSON.parse(res)
    //         updateState({ shopingCard: cart })
    //     })
    // }

    async function getCart(keys, loc) {
        let headers = {
            client: keys?.client,
            uid: keys?.uid,
            access_token: keys?.accessToken
        }

        await axios.get(`${baseURL}/shopping_cart/my_carts`, {
            headers: headers,
            params: {
                locale: loc.toLowerCase()
            },
        })
            .then((response) => {
                const { data } = response
                console.log('cartList', data);
                if (data) {
                    updateState({ cartList: data?.shopping_carts, loading: false, })
                }

            }).catch((error) => {
                console.log('error ', error);
                updateState({ loading: false })
            })
    }

    function cashOnDelivery() {
        updateState({
            viaCard: false,
            ondelivery: true
        })
    }
    function payViaCard() {
        updateState({
            viaCard: true,
            ondelivery: false
        })
    }

    function openDeleteModal(item, index) {
        updateState({
            showDeleteModal: true,
            selectedCart: item,
            cartIndex: index
        })
    }

    function handleIncrement() {
        let qty = shopingCard.qty
        qty = qty + 1
        shopingCard.qty = qty
        updateState({ shopingCard: shopingCard })
    }
    function handleDecrement() {
        let qty = shopingCard.qty
        qty = qty - 1
        shopingCard.qty = qty
        updateState({ shopingCard: shopingCard })
    }


    const renderCart = ({ item, index }) => {
        return (
            <Cart
                handleDelete={openDeleteModal}
                cartData={item}
                index={index}
                handleIncrement={handleIncrement}
                handleDecrement={handleDecrement}
                defaultLocal={_defaultLocal}
            />
        )
    }

    function renderCardFooter() {
        return (
            <View>
                {Delivery()}
                <View style={styles.btnView}>
                    <AppButton
                        title={`${_defaultLocal.checkout}`}
                        onPress={() => navigation.navigate('AppStack', {
                            screen: 'OrderPlaced'
                        })}
                        // onPress={() => { updateState({ showPaymentModal: !showPaymentModal }) }}
                        gradientBtnStyle={styles.gradientBtnStyle}
                    />
                    <AppButton
                        title={_defaultLocal.continue_shopping}
                        onPress={() => { }}
                        btnColor1={linLightFerozi}
                        btnColor2={linDarkFerozi}
                    />
                </View>
            </View>

        )
    }





    function Delivery() {
        let subtotal = Number(shopingCard.qty) * Number(shopingCard.price)
        let DC = 12
        let total = subtotal + DC

        return (
            <View>
                <View style={styles.deliveryView}>
                    <View style={styles.editView}>
                        <Text style={styles.delivery}>{`${_defaultLocal.delivery_city}`} / Time</Text>
                        <Icon
                            onPress={() => { updateState({ isVisible: !isVisible }) }}
                            name={'edit'} type={'entypo'}
                            iconStyle={styles.editIcon}
                        />
                    </View>
                    <HeaderText text={'Riyadh, KSA'} textStyle={styles.textStyle} />
                    <HeaderText text={'3 July, 2021 (03:00 PM - 07:30 PM)'} textStyle={{ ...styles.textStyle, color: ferozi }} />
                </View>

                <View style={{ ...styles.deliveryView, marginTop: hp(1) }}>
                    <View style={styles.editView}>
                        <Text style={styles.delivery}>{`${_defaultLocal.sub_total}`}</Text>
                        <Text style={styles.delivery}>{`$ ${subtotal}`}</Text>
                    </View>
                    <View style={{ ...styles.editView, marginTop: 2 }}>
                        <Text style={styles.delivery}>{_defaultLocal.delivery_charges}</Text>
                        <Text style={styles.delivery}>{`$ ${DC}`}</Text>
                    </View>
                    <View style={{ ...styles.editView, marginTop: 5 }}>
                        <Text style={{ ...styles.delivery, fontFamily: medium }}>{_defaultLocal.total}</Text>
                        <Text style={{ ...styles.delivery, color: red }}>{`$ ${total}`}</Text>
                    </View>

                </View>
            </View>
        )
    }

    function EditView() {
        return (
            <Overlay
                isVisible={isVisible}
                avoidKeyboard={true}
                useNativeDriver={true}
                onBackButtonPress={() => updateState({ isVisible: false })}
                onBackdropPress={() => updateState({ isVisible: false })}
                overlayStyle={styles.modalinnerView}>

                <View>
                    <Image
                        source={popup_edit}
                        style={styles.blueBGImg}
                    />
                    <View style={styles.middleText}>
                        <Text style={styles.largeText}>Edit Address, Date & Time Details</Text>
                    </View>
                    <View style={styles.modalConten}>
                        <View style={styles.reviewsBox}>
                            <View style={styles.reviewInnerView}>
                                <View style={{ alignSelf: 'flex-start', marginTop: 7, }}>
                                    <Icon
                                        name={'location-pin'}
                                        type={'entypo'}
                                        iconStyle={{ color: ferozi, fontSize: 25 }} />
                                </View>
                                <TextInput
                                    placeholder='Tell us how we can help you?'
                                    placeholderTextColor={grey}
                                    onChangeText={(text) => updateState({ reviews: text })}
                                    value={reviews}
                                    blurOnSubmit={false}
                                    multiline={true}
                                    style={styles.textInput}
                                />
                            </View>
                        </View>
                        <Text style={styles.validText}>Delivery Date</Text>
                        <FlatList
                            data={data}
                            keyExtractor={(item) => item.id}
                            renderItem={renderItem}
                            numColumns={3}
                            scrollEnabled={false}
                            columnWrapperStyle={{ justifyContent: 'space-between' }}
                        />

                        <Text style={{ ...styles.validText, marginTop: 5 }}>Delivery Time</Text>
                        <FlatList
                            data={data}
                            keyExtractor={(item) => item.id}
                            renderItem={renderItem}
                            numColumns={3}
                            scrollEnabled={false}
                            columnWrapperStyle={{ justifyContent: 'space-between' }}
                        />

                    </View>

                    <AppButton
                        title='Submit'
                        btnColor1={linLightFerozi}
                        btnColor2={linDarkFerozi}
                        onPress={() => updateState({ isVisible: !isVisible })}
                        gradientBtnStyle={styles.gradientBtnSubmit}
                    />

                </View>
            </Overlay>
        )
    }

    function DeliveryDate() {
        return (
            <View style={styles.DateView}>
                <Text style={styles.day}>Today</Text>
                <Text style={styles.time}>03 july</Text>
            </View>

        )
    }

    const renderItem = ({ item }) => {
        return (DeliveryDate())
    }

    function paymentModal() {
        return (
            <Modal
                isVisible={showPaymentModal}
                avoidKeyboard={true}
                useNativeDriver={true}
                onBackButtonPress={() => updateState({ showPaymentModal: false })}
                onBackdropPress={() => updateState({ showPaymentModal: false })}
            >

                <View style={styles.modalinnerView}>
                    <Image
                        source={popup_card}
                        style={styles.blueBGImg}
                    />
                    <View style={styles.middleText}>
                        <Text style={styles.largeText}>Select Payment Method</Text>
                    </View>
                    <View style={styles.modalConten}>
                        <Pressable
                            onPress={cashOnDelivery}
                            style={styles.radioBtnView}>
                            <View style={styles.radioBtnInnerView}>
                                <View style={ondelivery ? { ...styles.outerCircle, borderColor: ferozi } : { ...styles.outerCircle }}>
                                    <View style={ondelivery ? { ...styles.innerCircle, backgroundColor: ferozi } : { ...styles.innerCircle }}></View>
                                </View>
                                <Text style={styles.textStyle}>Cash on Delivery</Text>
                            </View>
                        </Pressable>

                        <Pressable
                            onPress={payViaCard}
                            style={styles.radioBtnView}>
                            <View style={styles.radioBtnInnerView}>
                                <View style={viaCard ? { ...styles.outerCircle, borderColor: ferozi } : { ...styles.outerCircle }}>
                                    <View style={viaCard ? { ...styles.innerCircle, backgroundColor: ferozi } : { ...styles.innerCircle }}></View>
                                </View>
                                <Text style={styles.textStyle}>Pay Using Card</Text>
                            </View>
                        </Pressable>
                    </View>
                    <AppButton
                        title='DONE'
                        btnColor1={linLightFerozi}
                        btnColor2={linDarkFerozi}
                        onPress={() => {
                            updateState({ showPaymentModal: !showPaymentModal })
                            navigation.navigate('AppStack', {
                                screen: 'YourCards'
                            })
                        }}
                        gradientBtnStyle={styles.gradientBtnCheckOut}
                    />
                </View>


            </Modal>
        )
    }

    function DeleteModal() {
        return (
            <Modal
                isVisible={showDeleteModal}
                avoidKeyboard={true}
                useNativeDriver={true}
                onBackButtonPress={() => updateState({ showDeleteModal: false })}
                onBackdropPress={() => updateState({ showDeleteModal: false })}
            >
                <View style={styles.modalinnerView}>
                    <Image source={delete_popup} style={styles.blueBGImg} />
                    <View style={styles.crossIcon}>
                        <Icon
                            onPress={() => { updateState({ showDeleteModal: !showDeleteModal }) }}
                            name={'cross'} type={'entypo'}
                            iconStyle={{ fontSize: 20, color: white }}
                        />
                    </View>
                    <View style={styles.middleText}>
                        <Text style={{ ...styles.largeText, color: black }}>Are you sure you want to delete?</Text>
                    </View>
                    <View style={styles.row}>
                        <AppButton
                            title='No'
                            btnColor1={white}
                            btnColor2={white}
                            btnText={{ ...styles.btnText, color: ferozi, marginVertical: 3 }}
                            onPress={() => { updateState({ showDeleteModal: !showDeleteModal }) }}
                            gradientBtnStyle={{
                                ...styles.actionBtn,
                                borderColor: ferozi,
                                borderWidth: 1,
                            }} />
                        <AppButton
                            title='Yes'
                            onPress={handleDeleteCart}
                            btnColor1={linLightFerozi}
                            btnColor2={linDarkFerozi}
                            btnText={styles.btnText}
                            gradientBtnStyle={styles.actionBtn}
                        />
                    </View>
                </View>
            </Modal>
        )
    }

    async function handleDeleteCart() {
        updateState({ processing: true })
        let headers = {
            client: accessKeys?.client,
            uid: accessKeys?.uid,
            access_token: accessKeys?.accessToken
        }
        let _data = {
            cart_id: selectedCart.cart_id
        }

        await axios.delete(`${baseURL}/shopping_cart/delete`, {
            headers: headers,
            data: _data
        })
            .then((response) => {
                const { data } = response
                if (data) {
                    let tempCartList = cartList
                    tempCartList.splice(cartIndex, 1)
                    updateState({
                        cartList: tempCartList,
                        processing: false,
                        showDeleteModal: !showDeleteModal
                    })
                } else {
                    updateState({ showDeleteModal: !showDeleteModal })
                    alertMsg('Something went wrong')
                }
            }).catch((error) => {
                updateState({ showDeleteModal: !showDeleteModal })
                console.log('error cart delete', error)
                alertMsg('Something went wrong')
            })

    }





    return (
        <View style={styles.wraper}>
            <AppHeader
                title={`${_defaultLocal.my_cart}`}
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                {cartList?.length > 0 ?

                    <FlatList
                        data={cartList}
                        keyExtractor={(item) => item.id}
                        renderItem={renderCart}
                        ListFooterComponent={renderCardFooter}
                        showsVerticalScrollIndicator={false}
                    />
                    :
                    <View style={styles.empityView}>
                        <Text>No Cart Found</Text>
                    </View>

                }


                {/* <View style={styles.btnView}>
                    <AppButton
                        title={'CHECKOUT'}
                        onPress={() => { updateState({ showPaymentModal: !showPaymentModal }) }}
                        gradientBtnStyle={styles.gradientBtnStyle}
                    />
                    <AppButton
                        title={'CONTINUE SHOPPING'}
                        onPress={() => { }}
                        btnColor1={linLightFerozi}
                        btnColor2={linDarkFerozi}
                    />
                </View> */}

            </View>

            {EditView()}
            {paymentModal()}
            {DeleteModal()}




            <DialogBox ref={dialogboxRef} />
        </View>
    )
}

export default MyCart
