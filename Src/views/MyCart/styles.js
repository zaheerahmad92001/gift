import { StyleSheet, } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { black, darkBlue, darkGrey, dark_offwhite, ferozi, grey, lightBlack, lightGrey, red, white } from '../../constants/colors'
import { regular, semi_bold } from '../../constants/fontsFamily'
import { appMarginHorizontal, big_tiny, large, small, statusBar, tiny } from '../../constants/appFontSize'



const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginTop:hp(3),
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    headerView: {
        alignSelf: "flex-start",
        marginTop: hp(4),
    },
    header: {
        fontFamily: semi_bold,
        fontSize: large,
        color: darkBlue
    },
   
    textView: {
        marginTop: hp(5),
    },
    congrats: {
        textAlign: "center",
        fontFamily: semi_bold,
        fontSize: large,
        color: darkBlue
    },
    textStyle: {
      textAlign:'left',
      color:red,
      fontSize:big_tiny,
    },
    contentView: {
        marginTop: hp(2)
    },

    signUpBtnText: {
        color: darkBlue,
        margin: 8
    },
    signUpBtnStyle: {
        width: wp(70),
        marginTop: hp(2),
        borderColor: darkBlue,
        borderWidth: 2,
    },
    gradientBtnStyle: {
        marginBottom:hp(2),
    },
    btnView:{
            // position:'absolute',
            // bottom:30,
            // alignSelf:'center',
            marginTop:10,
    },
    deliveryView:{
        // marginTop:hp(10),
        marginTop:hp(2),
        backgroundColor:dark_offwhite,
        paddingHorizontal:10,
        paddingVertical:10,
        borderRadius:10,
    },
    editView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    delivery:{
        color:black,
        fontSize:big_tiny,
        fontFamily:regular,
    },
    editIcon:{
        fontSize:20,
        color:ferozi,
    },
    modalinnerView: {
        backgroundColor: white,
        borderRadius: 20,
        width: wp(90),
        //   width:hp(80)
    },
    blueBGImg: {
        width:80,
        height: 80,
        marginTop: -40,
        alignSelf:'center',
        resizeMode: 'contain'
    },
    middleText: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    largeText: {
        color:red,
        marginTop:hp(1),
        fontFamily:regular,
        fontSize:small,
    },
    textInput:{
        paddingLeft:2,
        flex:1,
        paddingVertical:12,
        fontFamily:regular,
        fontSize:small,
        color:black,
    },
    modalConten: {
        marginHorizontal: wp(7),
        marginBottom: hp(2),
        marginTop: hp(2),
    },
    reviewsBox:{
        height:hp(13),
        textAlignVertical:'top',
        justifyContent:'flex-start',
        shadowColor:'#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        borderRadius:5,
    },
    reviewInnerView:{
       flexDirection:'row',
       alignItems:'center',
    },
    validText: {
        color: black,
        fontSize:big_tiny,
        fontFamily: regular,
        marginTop:hp(2),
    },
    DateView:{
        shadowColor:'#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        borderRadius:5,
        paddingVertical:5,
        paddingHorizontal:15,
        borderRadius:20,
        // width:wp(20),
        alignSelf:'center',
        justifyContent:'center',
        marginBottom:10,
        marginTop:10,
    },
    day:{
        color:black,
        fontSize:tiny,
        fontFamily:regular,
    },
    time:{
        color:black,
        fontSize:big_tiny,
        fontFamily:regular,
    },
    gradientBtnSubmit:{
        width:wp(40),
        marginBottom:10,
    },
    radioBtnView:{
        shadowColor:red,
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor: white,
        elevation: 2,
        paddingVertical:15,
        marginBottom:10,
        paddingLeft:10,
    },
    radioBtnInnerView:{
       flexDirection:'row',
       alignItems:'center',
    },
    outerCircle:{
       backgroundColor:white,
       width:20,
       height:20,
       borderRadius:20/2,
       borderColor:grey,
       borderWidth:1,
       justifyContent:'center',
       alignItems:'center',
    },
    innerCircle:{
      backgroundColor:grey,
      width:10,
      height:10,
      borderRadius:10/2,
      borderColor:grey,
      borderWidth:1,
    },
    textStyle:{
        marginLeft:10,
        color:black,
        fontFamily:regular,
        fontSize:small,
    },
    gradientBtnCheckOut:{
        width: wp(40),
        marginBottom:hp(3)
        
    },
    crossIcon:{
        width:20,
        height:20,
        borderRadius:20/2,
        backgroundColor:darkGrey,
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        right:10,
        top:10,
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    actionBtn:{
        width:wp(25),
        marginTop:15,
        marginBottom:10,
       
    },
    btnText:{
        marginVertical:4,
    },
    empityView:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    }







})
export default styles