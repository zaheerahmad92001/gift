import { StyleSheet, } from 'react-native'
import { heightPercentageToDP as hp , widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { white, red, black, ferozi, grey, lightGrey, } from '../../constants/colors';
import { boldItalic, medium, regular, semi_bold } from '../../constants/fontsFamily'
import { appMarginHorizontal, big_tiny, ex_large, large, mediumSize, small, tiny } from '../../constants/appFontSize'

const imgHeight = 180


const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
imgView:{
    height: imgHeight,
    marginTop:10, 
    borderRadius:10,
    overflow:'hidden',
},
textStyle:{
 fontFamily:regular,
 fontSize:big_tiny,
 color:black,
 textAlign:'left',
 marginTop:10,
},
linkStyle:{
    color:ferozi,
    fontSize:big_tiny,
    fontFamily:regular,
    textAlign:'left',
},
priceStyle:{
    color:red,
    fontFamily:regular,
    fontSize:large,
},
btnView:{
 flexDirection:'row',
 alignItems:'center',
 marginTop:hp(3),
 justifyContent:'space-around',

},
gradientBtnStyle: {
    width: wp(40),
    shadowColor:ferozi,
    shadowOffset:{height:2,width:2},
    shadowOpacity:0.3,
    shadowRadius:2,
    backgroundColor:white,
    elevation:2,
    borderRadius:10,
    borderColor:lightGrey,
    borderWidth:0.5
},
secondBtn:{
    width: wp(40),
    shadowColor:ferozi,
    shadowOffset:{height:2,width:2},
    shadowOpacity:0.3,
    shadowRadius:2,
    backgroundColor:white,
    elevation:2,
    borderRadius:10,
    paddingVertical:13,
},
btnText:{
color:black,
fontSize:small,
fontFamily:regular,
textAlign:'center',
},
validText: {
    color: black,
    fontSize:big_tiny,
    fontFamily: regular,
    marginTop:hp(2),
},
deliverView:{
    shadowColor:ferozi,
    shadowOffset:{height:1,width:1},
    shadowOpacity:0.2,
    shadowRadius:2,
    backgroundColor:white,
    elevation:2,
    borderRadius:10,
    paddingVertical:10,
    paddingHorizontal:10,
    marginRight:10,

},
day:{
color:black,
fontFamily:regular,
fontSize:tiny
},
dateText:{
    color:black,
    fontSize:big_tiny,
    fontFamily:regular,
},

qtyInner:{
    flexDirection:'row',
    alignItems:'center',
    alignSelf:'center',
    marginTop:hp(3),
    marginBottom:10,
},
circle:{
    width:30,
    height:30,
    borderRadius:30/2,
    backgroundColor:grey,
    alignItems:'center',
    justifyContent:'center',
},
iconStyle:{
    color:white,
    fontSize:15,
},
nameText:{
    color:black,
    fontFamily:regular,
    fontSize:ex_large,
},
shopingCart:{
    color:white,
    fontSize:20
},
addToCartView:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
},
addText:{
    color:white,
    fontSize:small,
    fontFamily:regular,
    marginLeft:5,
},
cartBtn:{
    backgroundColor:ferozi,
    paddingHorizontal:10,
    paddingVertical:10,
    width:wp(45),
    alignSelf:'center',
    borderRadius:20,
},
modalinnerView:{
    backgroundColor:white,
    borderRadius:20,
    width:wp(90),
    paddingBottom:hp(4)
  },
  blueBGImg:{
    width:wp(90),
    height:180,
    marginTop:-20,
  resizeMode:'contain'
},
middleText:{
    position:'absolute',
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'center',
    marginTop:(180/3)
   },
   
   modalConten:{
    marginHorizontal:10,
    marginBottom:hp(2),
    marginTop:hp(2),
},
    largeText:{
        fontSize:large,
        fontFamily:semi_bold,
        color:white,
    },
    gradientBtnCart:{
        marginTop:20,
    },
    btnStyle:{
        paddingVertical:10,
        marginTop:10,
        borderRadius:5,
        paddingHorizontal:10,
        borderColor:lightGrey,
        borderWidth:0.5
    },
    dateText:{
        color:black,
        fontSize:small,
        fontFamily:regular,
    }
})
export default styles