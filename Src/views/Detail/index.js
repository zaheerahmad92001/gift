import React, { useReducer, useRef, useEffect } from 'react'
import { View, Image, Text, FlatList, Pressable, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from 'react-native-modal'
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment'
import axios from 'axios'


import AppHeader from '../../components/appHeader'
import { categories_slider, curve } from '../../constants/images';
import styles from './styles'
import _Card from '../../components/Card';
import { Icon } from 'react-native-elements';
import HeaderText from '../../components/headerText';
import AppButton from '../../components/appButton';
import { shopingCard, myCart, local, defaultLocal } from '../../constants/constantsValues';
import { black, linDarkFerozi, linLightFerozi, red, white } from '../../constants/colors';
import { baseURL, domain } from '../../Utility'
import { authKeys } from '../../constants/constantsValues'
import _DatePicker from '../../components/_DatePicker';
import LoginOverLay from '../../components/LoginOverlay';
import SignUpOverLay from '../../components/SignUpOverlay';


const imgHeight = 180

function Detail({ navigation, route }) {
    const params = route.params
    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            isVisible: false,
            qty: 1,
            d_date: '',
            d_time: '',
            myself: true,
            someOne: false,
            date: new Date(),
            _time: new Date(),
            deliveryDate: '',
            deliveryTime: '',
            isDatePicker_Visible: false,
            isTimePicker_Visible: false,
            accessKeys: '',
            processing: false,
            dateError: false,
            timeError:false,
            showAuth:false,
            showLogin:false,
            showSignUp:false,
            _local: '',
            _defaultLocal:'',

        }
    )
    const {
        isVisible,
        qty, d_date,
        d_time, myself,
        someOne,
        date,
        isDatePicker_Visible,
        isTimePicker_Visible,
        deliveryDate,
        _time,
        deliveryTime,
        accessKeys,
        processing,
        dateError,
        timeError,
        showLogin,
        showSignUp,
        showAuth,
        _local,
        _defaultLocal,
        
    } = state

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getKeys()
            getDefaultLoca()
           });
      return unsubscribe;
    }, [])

    async function getKeys() {
        await AsyncStorage.getItem(authKeys)
            .then(async(res) => {
                let keys = JSON.parse(res)
               await AsyncStorage.getItem(local)
               .then((result)=>{
                 let _local = JSON.parse(result)
                 updateState({ accessKeys: keys ,_local: _local })

               })

            })
    }

    async function getDefaultLoca(){
        await AsyncStorage.getItem(defaultLocal)
        .then((def)=>{
           let defa = JSON.parse(def)
           console.log('default', defa);
           updateState({_defaultLocal:defa.defaults})
        })
    }

    function handleIncrement() {
        updateState({ qty: qty + 1 })
    }

    function handleDecrement() {
        if (qty > 0) {
            updateState({ qty: qty - 1 })
        }
    }

    function buyMySelf() {
        updateState({
            myself: true,
            someOne: false,
        })
    }

    function buyForSomeOne() {
        updateState({
            myself: false,
            someOne: true,
        })
    }

    function handleDeliveryDate() {
        updateState({ isDatePicker_Visible: !isDatePicker_Visible })
    }

    function selectDate(date) {
        console.log('date is ', date)
        updateState({
            date: date,
        })
    }

    function handleDate() {
        updateState({ isDatePicker_Visible: false })

        let dt = date.getDate()
        if (dt < 10) {
            dt = '0' + dt
        }

        let month = date.getMonth() + 1
        if (month < 10) {
            month = '0' + month
        }
        let year = date.getFullYear()

        let bd = `${dt}-${month}-${year}`

        updateState({ deliveryDate: bd, dateError: false })
    }

    function hideDatePicker() {
        updateState({ isDatePicker_Visible: false })
    }



    function handleDeliveryTime() {
        updateState({ isTimePicker_Visible: !isTimePicker_Visible })
    }

    function selectTime(time) {
        console.log('time is ', time)
        updateState({
            _time: time,
        })
    }

    function handleTime() {
        updateState({ isTimePicker_Visible: false })
        if (_time) {
            let dateTime = moment.utc(_time).local().format('YYYY-MM-DD: HH:mm')
            let [date, time] = dateTime.split(' ')
            updateState({ deliveryTime: time , timeError:false })
        }
    }

    function hideTimePicker() {
        updateState({ isTimePicker_Visible: false })
    }



    function FlowerDetail() {
        return (
            <View>
                <View style={styles.imgView}>
                    <Image
                        source={{ uri: `${domain}${params?.attachments[0]?.url}` }}
                        style={{ height: undefined, width: undefined, flex: 1, }}
                    />
                </View>
                <HeaderText
                    text={params?.description}
                    textStyle={styles.textStyle}
                />
                <Pressable onPress={() => { }}>
                    <Text style={styles.linkStyle}>{`${_defaultLocal.availability} : ${_defaultLocal.in_stock}`}</Text>
                </Pressable>
                <Text style={styles.priceStyle}>{`$ ${params?.price}`}</Text>
            </View>
        )
    }

    function ActionButton() {
        return (
            <View style={styles.btnView}>
                <AppButton
                    onPress={() => { buyMySelf() }}
                    // title={'Buy Gift for Myself'}
                    title={_defaultLocal.buy_gift_for_myself}
                    btnColor1={myself ? linLightFerozi : white}
                    btnColor2={myself ? linDarkFerozi : white}
                    btnText={{ color: myself ? white : black }}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
                <AppButton
                    onPress={() => { buyForSomeOne() }}
                    // title={'Buy Gift for Someone'}
                    title={_defaultLocal.buy_gift_for_someone}
                    btnColor1={someOne ? linLightFerozi : white}
                    btnColor2={someOne ? linDarkFerozi : white}
                    btnText={{ color: someOne ? white : black }}
                    gradientBtnStyle={styles.gradientBtnStyle}
                />
                {/* <Pressable 
                onPress={buyForSomeOne}
                style={styles.secondBtn}>
                    <Text style={styles.btnText}>Buy Gift for Someone</Text>
                </Pressable> */}
            </View>
        )
    }

    function Delivery() {
        return (
            <View>
                <Text style={dateError ?[styles.validText,{color:red}]: [styles.validText]}>
                    {_defaultLocal.deliver_date}
                    </Text>
                <Pressable
                    onPress={handleDeliveryDate}
                    style={dateError?[styles.btnStyle,{borderColor:red}]: [styles.btnStyle]}
                >
                    {deliveryDate ?
                        <Text style={styles.dateText}>{deliveryDate}</Text> :
                        <Text style={styles.dateText}>{_defaultLocal.select_date}</Text>
                    }
                </Pressable>
                {/* <FlatList
                    data={data}
                    keyExtractor={(item) => item.id}
                    renderItem={renderItem}
                    horizontal={true}
                    scrollEnabled={true}
                    style={{ marginTop: 10 }}
                    showsHorizontalScrollIndicator={false}
                /> */}

                <Text style={
                 timeError?{ ...styles.validText, marginTop: 10 , color:red }:
                 { ...styles.validText, marginTop: 10 ,} }>{_defaultLocal.delivery_time}</Text>

                <Pressable
                    onPress={handleDeliveryTime}
                    style={timeError?[styles.btnStyle,{borderColor:red}]: [styles.btnStyle]}
                >
                    {deliveryTime ?
                        <Text style={styles.dateText}>{deliveryTime}</Text> :
                        <Text style={styles.dateText}>{_defaultLocal.select_time}</Text>
                    }
                </Pressable>
                {/* <FlatList
                    data={data}
                    keyExtractor={(item) => item.id}
                    renderItem={renderItem}
                    horizontal={true}
                    scrollEnabled={true}
                    style={{ marginTop: 15 }}
                    showsHorizontalScrollIndicator={false}
                /> */}
            </View>
        )
    }

    function QtyBtn() {
        return (
            <View>
                <View style={styles.qtyInner}>
                    <Pressable
                        style={styles.circle}
                        onPress={handleDecrement}
                    >
                        <Icon
                            name='minus'
                            type='antdesign'
                            iconStyle={styles.iconStyle}
                        />
                    </Pressable>
                    <Text style={{ ...styles.nameText, marginHorizontal: 15, }}>{qty}</Text>
                    <Pressable
                        onPress={handleIncrement}
                        style={{ ...styles.circle, backgroundColor: red }}>
                        <Icon
                            name='plus'
                            type='antdesign'
                            iconStyle={styles.iconStyle}
                        />
                    </Pressable>
                </View>

                <Pressable
                    // onPress={openLogin}
                    onPress={() => { addToCart() }}
                    // onPress={() => { navigation.navigate('AuthStack') }}
                    style={styles.cartBtn}>
                    <View style={styles.addToCartView}>
                        <Icon
                            name={'shopping-cart'}
                            type={'entypo'}
                            iconStyle={styles.shopingCart}
                        />
                        <Text style={styles.addText}>{_defaultLocal.add_to_cart}</Text>
                    </View>
                </Pressable>
            </View>
        )
    }

    function addToCart() {
        const params = route.params

        let error = false
        if (!deliveryDate.length>0) {
            error = true
            updateState({ dateError: true })
        }
        if(!deliveryTime.length>0){
            error = true
            updateState({ timeError: true })
        }
        if(!error){

            setTimeout(() => {
                if(accessKeys){
                    updateState({isVisible: !isVisible  })
                }else{
                    navigation.navigate('AuthStack')
                }
            },500);

            
        }
    
        shopingCard.title = params.title
        shopingCard.image = params.attachment
        shopingCard.price = params?.price
        shopingCard.desc = params.description
        shopingCard.qty = qty
        shopingCard.delivery_date = d_date
        shopingCard.delivery_time = d_time
    }

    async function goToCart() {
        updateState({ processing: true })
        await AsyncStorage.setItem(myCart, JSON.stringify(shopingCard))

        let headers = {
            client: accessKeys?.client,
            uid: accessKeys?.uid,
            access_token: accessKeys?.accessToken
        }

        let _data = {
            product_id: params.id,
            stock: qty,
            delivery_date: deliveryDate,
            delivery_time: _time,
            buy_for_myselft: myself,
            buy_for_someone: someOne
        }
  
        if (!dateError) {
            await axios.post(`${baseURL}/shopping_cart/create`, _data, {
                headers: headers
            }).then((response) => {
                const { data } = response

                console.log('redposne', data);
                
                if (data?.api_status) {
                    setTimeout(() => {
                    updateState({ isVisible: !isVisible, processing: false })
                    navigation.navigate('BottomTabs', {
                        screen: 'MyCart',
                    })
                      },500)
                }
            }).catch((error) => {
                console.log('error ', error);
                updateState({ loading: false })
            })
        }
    }


    function AddItem() {
        return (
            <Modal
                isVisible={isVisible}
                avoidKeyboard={true}
                useNativeDriver={true}
                onBackButtonPress={() => updateState({ isVisible: false })}
                onBackdropPress={() => updateState({ isVisible: false })}
            >

                <View style={styles.modalinnerView}>
                    <Image
                        source={curve}
                        style={styles.blueBGImg}
                    />
                    <View style={styles.middleText}>
                        <Text style={styles.largeText}>{_defaultLocal.item_added}</Text>
                    </View>
                    <View style={styles.modalConten}>
                        <HeaderText
                            text={'Item has been added to your cart successfully!'}
                        />
                    </View>
                    <AppButton
                        title={_defaultLocal.continue_shopping}
                        onPress={() => {
                            updateState({ isVisible: !isVisible })
                            setTimeout(() => {
                            }, 1000)
                        }}
                        gradientBtnStyle={styles.gradientBtnCheckOut}
                    />
                    {processing ?
                        <View style={{ marginTop: 20, }}>
                            <ActivityIndicator color={red} size={'small'} />
                        </View> :
                        <AppButton
                            title={`${_defaultLocal.go_to_my_cart}`}
                            btnColor1={linLightFerozi}
                            btnColor2={linDarkFerozi}
                            onPress={() => { goToCart() }}
                            gradientBtnStyle={styles.gradientBtnCart}
                        />
                    }
                </View>


            </Modal>
        )
    }


    const renderItem = () => {
        return (
            <View style={styles.deliverView}>
                <Text style={styles.day}>Today</Text>
                <Text style={styles.dateText}>03 July</Text>
            </View>
        )
    }



    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                title={`${params?.title}`}
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {FlowerDetail()}
                    {ActionButton()}
                    {Delivery()}
                    {QtyBtn()}
                    {AddItem()}
                </KeyboardAwareScrollView>

            </View>
{/* {showAuth &&
<LoginOverLay
 isVisible={showLogin}
 openLogin={openLogin}
 openSignUp={openSignUp}
 showLogin={showLogin}
 showSignUp={showSignUp}
/>
} */}

{/* {showSignUp &&
<SignUpOverLay
 isVisible = {showSignUp}
/>
} */}



            <_DatePicker
                date={date}
                isVisible={isDatePicker_Visible}
                onDateChange={selectDate}
                onSelect={handleDate}
                onClose={hideDatePicker}
                minimumDate={new Date()}
            />

            <_DatePicker
                date={_time}
                isVisible={isTimePicker_Visible}
                onDateChange={selectTime}
                onSelect={handleTime}
                onClose={hideTimePicker}
                // minimumDate={new Date()}
                mode={'time'}
            />
        </View>
    )
}

export default Detail