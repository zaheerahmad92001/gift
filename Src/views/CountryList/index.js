import React, { useReducer, useRef } from 'react'
import { View, Image, Text, FlatList } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


import AppHeader from '../../components/appHeader'
import { country_img, gift_pack , saudia_flag } from '../../constants/images';
import styles from './styles'
import _Card from '../../components/Card';
import { Icon } from 'react-native-elements';
import HeaderText from '../../components/headerText';


const data = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
]

function CountryList({ navigation }) {

    const nameRef = useRef(null)
    const cardRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            viaCard: true,
            name: '',
            cardNum: '',
            isVisible: false,
        }
    )
    const { isVisible, name, cardNum } = state

    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <View style={styles.imgStyle}>
                    <Image
                        source={gift_pack}
                        style={{ width: undefined, height: undefined, flex: 1 }} />
                </View>
                <HeaderText
                    text={'Send a Gift to:'}
                    textStyle={styles.textStyle}
                />
            </View>
        )
    }

    function TopCities() {
        return (
            <View>
                <HeaderText
                    text={'TOP CITIES'}
                    textStyle={styles.topCities}
                />
                <FlatList
                    data={data}
                    keyExtractor={(item) => item.id}
                    renderItem={renderItem}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    />
                     <FlatList
                    data={data}
                    keyExtractor={(item) => item.id}
                    renderItem={renderItem}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    />
            </View>
        )
    }

    function Countries(){
        return(
            <View>
                <HeaderText
                text={'ALL COUNTRIES / CITIES'}
                textStyle={styles.allCountry}
                />
                <View style={styles.list}>
                  <View style={styles.innerView}>
                    <View style={styles.flag}>
                     <Image
                     source={saudia_flag}
                     style={{ width: undefined, height: undefined, flex: 1 }}
                     />
                    </View>
                    <HeaderText
                      text={'Saudi Arabia'}
                      textStyle={styles.cNameStyle}
                     />
                  </View>
                  <Icon
                   name={'down'}
                   type={'antdesign'}
                   iconStyle={styles.arrowDown}
                  />
                </View>
            </View>
        )
    }

    const renderItem = ({ item }) => {
        return (
            <View style={styles.cityView}>
                <View style={styles.countryimgStyle}>
                    <Image
                        source={country_img}
                        style={{ width: undefined, height: undefined, flex: 1 }} />
                </View>
                <View style={styles.cityNameView}>
                    <Text style={styles.nameStyle}>Lahore</Text>
                </View>
            </View>
        )
    }



    return (
        <View style={styles.wraper}>
            <AppHeader
                leftPress={() => navigation.pop()}
                iconName='chevron-back'
                iconType='ionicon'
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView>
                    {UserImgUI()}
                    {TopCities()}
                    {Countries()}
                </KeyboardAwareScrollView>

            </View>



        </View>
    )
}

export default CountryList