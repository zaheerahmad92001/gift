import React, { useEffect, useReducer, useRef } from 'react'
import { View, Text, Pressable, TextInput, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import axios from 'axios'
import DialogBox from 'react-native-dialogbox';

import AppButton from '../../components/appButton'
import HeaderText from '../../components/headerText'
import styles from './styles'
import BackButton from '../../components/backButton'
import InputField from '../../components/inputField'
import { ValidateEmail } from '../../Utility/RandomFun'
import { baseURL } from '../../Utility'
import { red } from '../../constants/colors'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { defaultLocal } from '../../constants/constantsValues';

function SignUp({ navigation }) {

    let dialogboxRef = useRef()
    const nameRef = useRef(null)
    const emailRef = useRef(null)
    const contactRef = useRef(null)
    const passwordRef = useRef(null)
    const confirmPassRef = useRef(null)

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            name: '',
            nameError: false,
            contact: '',
            contactError: false,
            email: "",
            emailError: false,
            password: '',
            passwordError: false,
            confirmPass: '',
            confirmPassError: false,
            securePass: true,
            secureConfirmPass: true,
            loading: false,
            _defaultLocal: '',
        }
    )
    const {
        name,
        nameError,
        email,
        emailError,
        password,
        contact,
        contactError,
        passwordError,
        confirmPass,
        confirmPassError,
        securePass,
        secureConfirmPass,
        loading,
        _defaultLocal,

    } = state

    useEffect(() => {
        getDefaultLoca()

    },[])


    async function getDefaultLoca() {
        await AsyncStorage.getItem(defaultLocal)
            .then((def) => {
                let defa = JSON.parse(def)
                updateState({ _defaultLocal: defa.defaults })
            })
    }

    function alertMsg(msg1, msg2) {
        dialogboxRef.current.tip({
            title: 'Message',
            content: [
                msg1,
                msg2
            ],
            btn: { text: 'OK', callback: () => { } },
        })

    }


    function hideShowPassword() {
        updateState({ securePass: !securePass })
    }
    function hideShowConfirmPassword() {
        updateState({ secureConfirmPass: !secureConfirmPass })
    }

    const formValidation = () => {
        let error = false
        if (name.trim().length < 1) {
            updateState({ nameError: true })
            error = true
        }
        if (!ValidateEmail(email)) {
            updateState({ emailError: true })
            error = true
        }
        if (contact.trim().length < 1) {
            updateState({ contactError: true })
            error = true
        }
        if (password?.trim().length < 6) {
            updateState({ passwordError: true })
            error = true
        }
        if (confirmPass != password || confirmPass == '') {
            updateState({ confirmPassError: true })
            error = true
        }
        return error
    }

    function handleSignup() {
        let validate = formValidation()
        if (!validate) {
            updateState({ loading: true })

            let _data = {
                email: email.toLowerCase(),
                password: password,
                password_confirmation: confirmPass,
                username: name,
                first_name: name,
                contact_number: contact
                // last_name: "zaidi"
            }
            axios.post(`${baseURL}/users.json`, _data).then(async (response) => {
                const { data } = response
                if (data) {
                    navigation.navigate('Login')
                    clearForm()
                    console.log('response', data);
                }
            }).catch((error) => {
                updateState({ loading: false })
                alertMsg('Something went wrong')
                console.log('error', error);
            })
        }
    }

    function clearForm() {
        updateState({
            loading: false,
            name: '',
            email: '',
            contact: '',
            password: '',
            confirmPass: '',
        })
    }


    return (
        <View style={styles.wraper}>
            <View style={styles.container}>
                <BackButton 
                text={_defaultLocal.back}
                onPress={() => { navigation.pop() }} />
                <KeyboardAwareScrollView>
                    <View style={styles.headerView}>
                        <Text style={styles.header}>{_defaultLocal.sign_up}</Text>
                    </View>
                    <View style={styles.textView}>
                        <HeaderText
                            text={_defaultLocal.sign_up_to_create_your_account}
                            textStyle={styles.textStyle} />
                    </View>
                    <View style={styles.contentView}>
                        <InputField
                            placeholder={_defaultLocal.name}
                            onChangeText={(text) => { updateState({ name: text, nameError: false }) }}
                            value={name}
                            keyboardType={'default'}
                            ref={nameRef}
                            onSubmitEditing={() => emailRef?.current?.focus()}
                            blurOnSubmit={false}
                            error={nameError}
                        />
                        {nameError &&
                            <Text style={styles.errorText}>{'please enter your name'}</Text>
                        }
                        <InputField
                            placeholder={_defaultLocal.email}
                            onChangeText={(text) => { updateState({ email: text, emailError: false }) }}
                            value={email}
                            keyboardType={'email-address'}
                            inputContainerStyle={styles.inputContainerStyle}
                            ref={emailRef}
                            onSubmitEditing={() => contactRef?.current?.focus()}
                            blurOnSubmit={false}
                            error={emailError}

                        />
                        {emailError &&
                            <Text style={styles.errorText}>{'please enter valid name'}</Text>
                        }
                        <InputField
                            placeholder={_defaultLocal.contact_number}
                            onChangeText={(text) => { updateState({ contact: text, contactError: false }) }}
                            value={contact}
                            keyboardType={'number-pad'}
                            inputContainerStyle={styles.inputContainerStyle}
                            ref={contactRef}
                            onSubmitEditing={() => passwordRef?.current?.focus()}
                            blurOnSubmit={false}
                            error={contactError}
                        />
                        {contactError &&
                            <Text style={styles.errorText}>{'please enter phone number'}</Text>
                        }
                        <InputField
                            placeholder={_defaultLocal.password}
                            onChangeText={(text) => { updateState({ password: text, passwordError: false }) }}
                            value={password}
                            keyboardType={'default'}
                            secureTextEntry={securePass}
                            hideShowPassword={hideShowPassword}
                            passIconName={securePass ? 'eye-with-line' : 'eye'}
                            passIconType={'entypo'}
                            inputContainerStyle={styles.inputContainerStyle}
                            ref={passwordRef}
                            onSubmitEditing={() => confirmPassRef?.current?.focus()}
                            blurOnSubmit={false}
                            error={passwordError}
                        />
                        {passwordError &&
                            <Text style={styles.errorText}>{'password must be greate than 6 characters'}</Text>
                        }
                        <InputField
                            placeholder={_defaultLocal.confirm_password}
                            onChangeText={(text) => { updateState({ confirmPass: text, confirmPassError: false }) }}
                            value={confirmPass}
                            keyboardType={'default'}
                            secureTextEntry={secureConfirmPass}
                            hideShowPassword={hideShowConfirmPassword}
                            passIconName={secureConfirmPass ? 'eye-with-line' : 'eye'}
                            passIconType={'entypo'}
                            inputContainerStyle={styles.inputContainerStyle}
                            ref={confirmPassRef}
                            returnKeyType={'done'}
                            // onSubmitEditing={()=>?.current?.focus()}
                            blurOnSubmit={false}
                            error={confirmPassError}
                        />
                        {confirmPassError &&
                            <Text style={styles.errorText}>{'password not match'}</Text>
                        }
                        {loading ?
                            <View style={styles.indicator}>
                                <ActivityIndicator
                                    color={red}
                                    size={'small'}
                                />
                            </View>
                            :
                            <AppButton
                                title={_defaultLocal.sign_up}
                                onPress={() => { handleSignup() }}
                                gradientBtnStyle={styles.gradientBtnStyle}
                            />
                        }
                        <View style={styles.sendAgainView}>
                            <Text style={styles.textStyle}>{_defaultLocal.already_have_an_account}?</Text>
                            <Pressable onPress={() => { navigation.navigate('Login') }}>
                                <Text style={styles.sendText}>Log in</Text>
                            </Pressable>
                        </View>

                        {/* <Text style={styles.ORText}>OR</Text>
                        <Text style={styles.otherMethod}>Sing in using</Text> */}

                        {/* <View style={styles.socialLoginView}>
                            <View style={styles.btnBG}>
                                <Icon name={'google'} type={'fontisto'} iconStyle={styles.iconStyle} tvParallaxProperties={undefined} />
                            </View>
                            <View style={styles.btnBG}>
                                <Icon name={'facebook'} type={'fontisto'} iconStyle={styles.iconStyle} tvParallaxProperties={undefined} />
                            </View>
                            <View style={styles.btnBG}>
                                <Icon name={'apple'} type={'fontisto'} iconStyle={styles.iconStyle} tvParallaxProperties={undefined} />
                            </View>
                        </View> */}
                    </View>
                </KeyboardAwareScrollView>
            </View>
            <DialogBox ref={dialogboxRef} />
        </View>
    )

}
export default SignUp

