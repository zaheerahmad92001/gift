import { StyleSheet, } from 'react-native'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { white, red, black, lightGrey, } from '../../constants/colors';
import { regular } from '../../constants/fontsFamily'
import { appMarginHorizontal, large, tiny } from '../../constants/appFontSize'

const imgHeight = 180


const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },

    searchView:{
        shadowColor:red,
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor:white,
        elevation: 2,
        // paddingVertical:5,
        marginTop:10,
        borderRadius:10,
        paddingHorizontal:10
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    largeText:{
        fontSize:large,
        fontFamily:regular,
        color:red,
    },
    filter:{
        width:25,
        height:25,
        resizeMode:'contain'
    },
    inputField:{
        flex:1,
        paddingVertical:10,
        paddingLeft:5,

    },

    cityView:{
        backgroundColor:white,
        shadowColor:red,
        shadowOffset:{height:2,width:2},
        shadowOpacity:0.2,
        shadowRadius:2,
        elevation:2,
        marginBottom:hp(2),
        borderRadius:10,
        paddingVertical:10,
        paddingHorizontal:5,
        marginRight:25,
        width:wp(25),
        // alignSelf:'center',
        // marginRight:wp(4)
      },

      countryimgStyle:{
        width:80,
        height:80,
        resizeMode:'contain',
        overflow:'hidden',
        borderRadius:10,
    },

    cityNameView:{
        top:hp(-3),
        zIndex:1,
        justifyContent:'center',
        alignItems:'center',
        // width:100,
    },
    nameStyle:{
      color:black,
      fontFamily:regular,
      fontSize:tiny,  
      textAlign:'center',
      
    },
    noAds:{ 
        height: imgHeight ,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:lightGrey
    }

})
export default styles