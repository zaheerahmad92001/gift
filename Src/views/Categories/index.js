import React, { useEffect, useReducer, useRef } from 'react'
import { View, Image, Text, FlatList , TextInput, Pressable, ActivityIndicator } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios'


import {authKeys, defaultLocal, local} from '../../constants/constantsValues'
import { baseURL, domain } from '../../Utility'
import AppHeader from '../../components/appHeader'
import styles from './styles'
import {filter , search , categories_slider , cake} from '../../constants/images'
import { lightGrey,red } from '../../constants/colors';
import { heightPercentageToDP as hp} from 'react-native-responsive-screen';

const imgHeight = 180

function Categories({ navigation }) {

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            searchValue:undefined,
            accessKeys:undefined,
            productList:undefined,
            productListCopy:undefined,
            loading:false,
            adsLoading:false,
            adsList:undefined,
            _local: '',
            _defaultLocal:'',

        }
    )
    const {
         searchValue ,
         accessKeys , 
         loading,
         adsLoading ,
         adsList,
         productList,
         productListCopy,
        _local,
        _defaultLocal,

        } = state

    useEffect(()=>{
        const unsubscribe = navigation.addListener('focus', () => {
          getKeys(getCategories , getAds)
          getDefaultLoca()
           });
      return unsubscribe;
      },[])

 async function getKeys(callback , callAds){
      await AsyncStorage.getItem(authKeys)
    .then( async(res)=>{
        let keys = JSON.parse(res)
        await AsyncStorage.getItem(local)
        .then((result) => {
            let _local = JSON.parse(result) ? JSON.parse(result) : 'EN'
            updateState({accessKeys:keys ,  _local: _local})
            callback(keys , _local)
            callAds(keys , _local)
        })
    })
   }

   async function getDefaultLoca(){
    await AsyncStorage.getItem(defaultLocal)
    .then((def)=>{
       let defa = JSON.parse(def)
    //    console.log('default', defa);
       updateState({_defaultLocal:defa.defaults})
    })
}

   async function getCategories(keys , loc){
       let headers ={
         client : keys?.client,
         uid: keys?.uid,
         access_token: keys?.accessToken
       }

    updateState({loading:true})
    // await axios.get(`${baseURL}/product/list_categories`,{headers: headers})
    await axios.get(`${baseURL}/product/list_products`,{
        headers: headers,
        params: {
            locale:loc.toLowerCase()
          },
    })
    .then((response)=>{
        const {data} = response
        if(data){
        updateState({
            productList:data.products,
            productListCopy:data.products,
            loading:false
        })
    }
    }).catch((error)=>{
        console.log('error ', error);
        updateState({loading:false})
    })
   }


async function getAds(keys){
    let headers ={
        client : keys?.client,
        uid: keys?.uid,
        access_token: keys?.accessToken
      }

 updateState({adsLoading:true})
 await axios.get(`${baseURL}/ad/fetch_ads`,{headers: headers})
 .then((response)=>{
    console.log('ads response',response);
    const {data} = response
    if(data?.api_status){
    updateState({
        adsList:data.ads,
        adsLoading:false
    })
}

}).catch((error)=>{
    console.log('error ', error);
    updateState({adsLoading:false})
})

}


    function SearchBar() {
        return (
            <View style={styles.searchView}>
                <View style={{ ...styles.row,  }}>
                    <TextInput
                        placeholder={_defaultLocal.search_here}
                        placeholderTextColor={lightGrey}
                        onChangeText={(text)=> handleSearch(text)}
                        value={searchValue}
                        style={styles.inputField}
                    />
                    <Image
                        source={search}
                        style={styles.filter}
                    />
                </View>
            </View>
        )
    }

const handleSearch=(text)=>{
  let tempList = JSON.parse(JSON.stringify(productList))
 
   const filterData = tempList.filter((obj)=>{
      const regex = new RegExp([text.trim()], 'i')
        if(obj.title.search(regex) >= 0){
          return obj
      }
   })
   
   if(text.trim().length > 0){
   updateState({productList:filterData, searchValue:text})
   }else{
   updateState({productList:productListCopy , searchValue:text})
   }
}


    function Products(){
        return(
            <>
            {!loading?
            <FlatList
            data={productList}
            keyExtractor={(item)=>item.id}
            renderItem={renderItem}
            numColumns={3}
            // columnWrapperStyle={{justifyContent: 'space-between'}}
            style={{marginTop:20}}
            extraData={productList}
            scrollEnabled={false}
            showsVerticalScrollIndicator={false}
            />
            :
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <ActivityIndicator
               color={red}
               size={'large'}
              />
            </View>
    }
            </>
        )
    }

    const renderItem = ({ item }) => {
        console.log('item', item);
        return (
            <Pressable 
            onPress={()=>navigation.navigate('AppStack',{
                screen:'Detail',
                params:item
            })}
            style={styles.cityView}>
                <View style={styles.countryimgStyle}>
                    <Image
                        // source={cake}
                        source={{uri:`${domain}${item?.attachments[0]?.url}`}}
                        style={{ width: undefined, height: undefined, flex: 1 }} />
                </View>
                <Text style={styles.nameStyle}>{
                 item?.title.length > 10 ?
                `${item?.title.substring(0,10)}...`: item?.title
                }</Text>
            </Pressable>
        )
    }
   
    function SwiperView(){
        return(
            <View style={{marginTop:10}}>
                {!adsLoading && adsList?.length>0?
            <Swiper
            loop={true}
            autoplay={true}
            autoplayTimeout={5}
            autoplayDirection={true}
            // dot={<View style={styles.dotStyle} />}
            // activeDot={<View style={styles.activeDotStyle} />}
            style={{ height: (imgHeight), }}
            containerStyle={{ height: imgHeight }}
        >
            {adsList?.map((item, index) => {
                return (
                    <View style={{ height: imgHeight }}>
                        <Image
                            source={{uri:item?.value}}
                            style={{ height: imgHeight, width: '100%' }}
                        />
                    </View>
                )
            })}
        </Swiper>
        :!adsLoading && adsList?.length==0?
        <View style={styles.noAds}><Text>No ads</Text></View>
        :
        <View style={{marginBottom:hp(10),marginTop:10,}}><ActivityIndicator color={red} size={'large'}/></View>
        }
        </View>
        )
    }

    return (
        <View style={styles.wraper}>
            <AppHeader
                title={`${_defaultLocal.products}`}
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView
                nestedScrollEnabled={true}
                showsVerticalScrollIndicator={false}
                >
                <View style={{...styles.row,marginTop:15,}}>
                    <Text style={styles.largeText}>{`${_defaultLocal.search_gifts}`}</Text>
                    <Image
                        source={filter}
                        style={styles.filter}
                    />
                </View>
                  {SearchBar()}
                  {SwiperView()}
                  {Products()}
                </KeyboardAwareScrollView>

            </View>



        </View>
    )
}

export default Categories