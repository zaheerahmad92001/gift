import React,{useEffect,}from 'react'
import {View, Text, Image , StyleSheet}from 'react-native'
import AppButton from '../../components/appButton'
import { linDarkFerozi, linLightFerozi, white } from '../../constants/colors'
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {appIcon} from '../../constants/images'
import {authKeys , user} from '../../constants/constantsValues'

function Splash({navigation}){

    useEffect(()=>{
      runApp()
    })

   async function runApp(){
    await AsyncStorage.getItem(authKeys)
          .then((res)=>{
              let keys = JSON.parse(res)
              console.log('splash key', keys);
                setTimeout(() => {
                    if(keys){
                    navigation.navigate('BottomTabs')
                    // navigation.navigate('LandingScreen')
                    }else{
                        navigation.navigate('BottomTabs')
                    // navigation.navigate('LandingScreen')
                    }
                },2000);
              
             
          })
         
        
    }

    return(
    <View style={styles.wraper}>
     <Image
     source={appIcon}
     style={styles.appIconStyle}
     >
     </Image>
    </View>
)
}
export default Splash

const styles  = StyleSheet.create({
    wraper:{
        flex:1,
        justifyContent:"center",
        alignItems:'center',
        backgroundColor:white
    },
    appIconStyle:{
        width:'100%',
        height:250,
        resizeMode:'contain'
    },
})