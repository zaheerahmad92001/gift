import React, { useReducer,useEffect } from 'react'
import { View, Text, Pressable, Image, TouchableOpacity } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { launchImageLibrary } from "react-native-image-picker"
import AsyncStorage from '@react-native-async-storage/async-storage';


import styles from './styles'
import {authKeys, defaultLocal, user} from '../../constants/constantsValues'
import { Icon } from 'react-native-elements'
import { 
    editprofile, 
    mycards, 
    myorders, 
    topay, 
    toreceive, 
    toship,
    buy_credit,
    share_credit
 } from '../../constants/images'
import { darkBlue, linDarkFerozi, linLightFerozi } from '../../constants/colors'
import AppHeader from '../../components/appHeader'
import { options } from '../../constants/constantsValues'
import SIcon from 'react-native-vector-icons/SimpleLineIcons';
import LinearGradient from 'react-native-linear-gradient'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen'


function MyProfile({ navigation }) {

    const [state, updateState] = useReducer(
        (state, newState) => ({ ...state, ...newState }),
        {
            filePath: '',
            accessKeys:'',
            User:'',
            _defaultLocal:'',

        }
    )
    const { 
        filePath,
         accessKeys,
          User ,
        _defaultLocal,

        } = state

    useEffect(()=>{
        const unsubscribe = navigation.addListener('focus', () => {
            getUser()
            getDefaultLoca()
        
           });
      return unsubscribe;
      },[])

     
      async function getDefaultLoca(){
        await AsyncStorage.getItem(defaultLocal)
        .then((def)=>{
           let defa = JSON.parse(def)
           updateState({_defaultLocal:defa.defaults})
        })
    }

 async function getUser(){
  await AsyncStorage.getItem(user).then((res)=>{
      let user = JSON.parse(res)
      updateState({User:user})
  })
 }



    function opneImagePicker() {

        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };
                updateState({
                    filePath: response.assets[0],
                    //  fileData: response.data,
                    //  fileUri: response.uri
                });
            }
        });
    }

    function UserImgUI() {
        return (
            <View style={styles.profileView}>
                <TouchableOpacity
                    style={styles.profileOuterView}
                    onPress={opneImagePicker}
                >
                    {!filePath ?
                        <SIcon name="user" style={{ fontSize: 40, color: darkBlue }} />
                        :
                        <View style={styles.imgStyle}>
                            <Image
                                source={{ uri: filePath.uri }}
                                style={{ width: undefined, height: undefined, flex: 1 }} />
                        </View>
                    }
                </TouchableOpacity>
                <Text style={styles.nameStyle}>{User?.first_name}</Text>
            </View>
        )
    }

    function myorderUI() {
        return (
            <View style={styles.contentView}>
                <View style={styles.cardView}>
                    <Text style={styles.heading}>{_defaultLocal.my_orders}</Text>
                    <View style={styles.cardContent}>
                        <Pressable style={styles.textView}>
                            <Image
                                source={topay}
                                style={styles.orderImg}
                            />
                            <Text style={styles.textStyle}>To Pay</Text>
                        </Pressable>
                        <Pressable style={styles.textView}>
                            <Image
                                source={toreceive}
                                style={styles.orderImg}
                            />
                            <Text style={styles.textStyle}>To Ship</Text>
                        </Pressable>
                        <Pressable style={styles.textView}>
                            <Image
                                source={toship}
                                style={styles.orderImg}
                            />
                            <Text style={styles.textStyle}>To Receive</Text>
                        </Pressable>
                    </View>
                </View>
            </View>
        )
    }


    function buttonUI() {
        return (
            <View>
                <Pressable
                    onPress={() => navigation.navigate('AppStack',{
                        screen:'YourCards'
                    })}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={mycards} style={{ width: 40, height: 40, resizeMode: 'contain' }} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.my_carts}</Text>
                        </View>
                        <Icon
                            name={'chevron-forward'}
                            type={'ionicon'}
                            iconStyle={styles.iconStyle}
                            tvParallaxProperties={undefined}
                        />
                    </View>
                </Pressable>

                <Pressable
                    onPress={() => navigation.navigate('AppStack',{
                        screen:'MyOrders'
                    })}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={myorders} style={styles.btnImg} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.my_orders}</Text>
                        </View>
                        <Icon
                            name={'chevron-forward'}
                            type={'ionicon'}
                            iconStyle={styles.iconStyle}
                            tvParallaxProperties={undefined}
                        />
                    </View>
                </Pressable>

                <Pressable
                    onPress={() => navigation.navigate('AppStack',{
                        screen:'EditProfile'
                    })}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={editprofile} style={{ width: 35, height: 35, resizeMode: 'contain' }} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.edit_profile}</Text>
                        </View>
                        <Icon
                            name={'chevron-forward'}
                            type={'ionicon'}
                            iconStyle={styles.iconStyle}
                            tvParallaxProperties={undefined}
                        />
                    </View>
                </Pressable>

                <Pressable
                    onPress={() => navigation.navigate('AppStack',{
                        screen:'GiftReceived'
                    })}
                    style={{ ...styles.buttoncardView }}>
                    <View style={styles.buttonCardContent}>
                        <View style={styles.leftSide}>
                            <Image source={editprofile} style={{ width: 35, height: 35, resizeMode: 'contain' }} />
                            <Text style={styles.buttonTitle}>{_defaultLocal.my_gifts}</Text>
                        </View>
                        <Icon
                            name={'chevron-forward'}
                            type={'ionicon'}
                            iconStyle={styles.iconStyle}
                            tvParallaxProperties={undefined}
                        />
                    </View>
                </Pressable>

            </View>

        )
    }
    function WalletUI() {
        return (
            <View style={styles.walletWraper}>
                <LinearGradient
                    start={{ x: 0.1, y: 0.1 }} end={{ x: 1, y: 1 }}
                    colors={[linLightFerozi, linDarkFerozi]}
                    style={[styles.linearGradient]}>
                    <Text style={[styles.buttonText]}>{`${_defaultLocal.my_wallet}`}</Text>
                    <Text style={[styles.creditText]}>{'$6574'}</Text>
                </LinearGradient>
                <View style={{marginLeft:10}}>
                    <Pressable
                        onPress={()=>{}}
                        style={{ 
                            ...styles.buttoncardView,
                             marginTop: hp(0),
                            paddingTop:5,
                            paddingBottom:5, }}>
                        <View style={styles.buttonCardContent}>
                            <View style={styles.leftSide}>
                                <Image source={buy_credit} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                <Text style={{...styles.buttonTitle,marginLeft:7}}>{_defaultLocal.buy_credit}</Text>
                            </View>
                            
                        </View>
                    </Pressable>
                    <Pressable
                        onPress={()=>{}}
                        style={{ 
                            ...styles.buttoncardView,
                             marginTop: hp(1),
                            paddingTop:5,
                            paddingBottom:5, }}>
                        <View style={styles.buttonCardContent}>
                            <View style={{...styles.leftSide}}>
                                <Image source={share_credit} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                <Text style={{...styles.buttonTitle,marginLeft:7}}>{_defaultLocal.share_credit}</Text>
                            </View>
                            
                        </View>
                    </Pressable>
                </View>
            </View>
        )
    }


    return (
        <View style={styles.wraper}>
            <AppHeader
                title='My Profile'
                //  iconName='chevron-back'
                //  iconType='ionicon'
                //  leftPress={()=>{navigation.goBack()}}
                rightIconName={'question'}
                rightIconType={'antdesign'}
                rightPress={() => { }}
            />
            <View style={styles.container}>
                <KeyboardAwareScrollView
                 showsVerticalScrollIndicator={false}
                >
                    {UserImgUI()}
                    {WalletUI()}
                    {myorderUI()}
                    {buttonUI()}
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default MyProfile
