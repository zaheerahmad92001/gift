export const  options = {
    title: 'Select Image',
    mediaType:'photo',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

export const authKeys ='AUTH_KEYS'
export const local = 'LOCAL'
export const defaultLocal = 'DEFALULT_LOCAL'

export const myCart='MYCART'
export const user = 'USER'
export const shopingCard={
  title:'',
  image:'',
  price:'',
  qty:'',
  desc:'',
  delivery_date:'',
  delivery_time:'',

}