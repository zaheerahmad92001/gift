export const appBG  = '#FFFFFF'
export const linLighRed ='#F54D83'
export const linDarkRed = '#CC1653'
export const red='#E3185A'
export const linLightFerozi = '#18D9D0'
export const linDarkFerozi = '#06958F'
export const ferozi ='#07A79F'
export const white ='#fff'
export const black ='#000'
export const lightBlack ='#1A2E35';
export const dark_offwhite = '#F5F5F5'
export const pink = 'rgb(252,227,235)'

export const darkGrey ='#5A5A5A';
export const lightGrey ='#CCCCCC';
export const grey ='#ADADAD';